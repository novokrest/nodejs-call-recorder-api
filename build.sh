set -e

rm -rf tmp/
./dev/package.sh
./dev/run-codegen.sh
./dev/install.sh
./dev/test.sh
