const CallRecorderApi = require('../main/CallRecorderApi');
const Configuration = require('../main/Configuration');
const App = require('../main/model/App');
const BuyCreditsRequest = require('../main/model/BuyCreditsRequest');
const BuyCreditsResponse = require('../main/model/BuyCreditsResponse');
const CloneFileRequest = require('../main/model/CloneFileRequest');
const CloneFileResponse = require('../main/model/CloneFileResponse');
const CreateFileData = require('../main/model/CreateFileData');
const CreateFileRequest = require('../main/model/CreateFileRequest');
const CreateFileResponse = require('../main/model/CreateFileResponse');
const CreateFolderRequest = require('../main/model/CreateFolderRequest');
const CreateFolderResponse = require('../main/model/CreateFolderResponse');
const DeleteFilesRequest = require('../main/model/DeleteFilesRequest');
const DeleteFilesResponse = require('../main/model/DeleteFilesResponse');
const DeleteFolderRequest = require('../main/model/DeleteFolderRequest');
const DeleteFolderResponse = require('../main/model/DeleteFolderResponse');
const DeleteMetaFilesRequest = require('../main/model/DeleteMetaFilesRequest');
const DeleteMetaFilesResponse = require('../main/model/DeleteMetaFilesResponse');
const DeviceType = require('../main/model/DeviceType');
const FilesPermission = require('../main/model/FilesPermission');
const GetFilesRequest = require('../main/model/GetFilesRequest');
const GetFilesResponse = require('../main/model/GetFilesResponse');
const GetFilesResponseFile = require('../main/model/GetFilesResponseFile');
const GetFoldersRequest = require('../main/model/GetFoldersRequest');
const GetFoldersResponse = require('../main/model/GetFoldersResponse');
const GetFoldersResponseFolder = require('../main/model/GetFoldersResponseFolder');
const GetLanguagesRequest = require('../main/model/GetLanguagesRequest');
const GetLanguagesResponse = require('../main/model/GetLanguagesResponse');
const GetMessagesRequest = require('../main/model/GetMessagesRequest');
const GetMessagesResponse = require('../main/model/GetMessagesResponse');
const GetMessagesResponseMsg = require('../main/model/GetMessagesResponseMsg');
const GetMetaFilesRequest = require('../main/model/GetMetaFilesRequest');
const GetMetaFilesResponse = require('../main/model/GetMetaFilesResponse');
const GetMetaFilesResponseMetaFiles = require('../main/model/GetMetaFilesResponseMetaFiles');
const GetPhonesRequest = require('../main/model/GetPhonesRequest');
const GetPhonesResponse = require('../main/model/GetPhonesResponse');
const GetPhonesResponsePhone = require('../main/model/GetPhonesResponsePhone');
const GetProfileRequest = require('../main/model/GetProfileRequest');
const GetProfileResponse = require('../main/model/GetProfileResponse');
const GetProfileResponseProfile = require('../main/model/GetProfileResponseProfile');
const GetSettingsRequest = require('../main/model/GetSettingsRequest');
const GetSettingsResponse = require('../main/model/GetSettingsResponse');
const GetSettingsResponseSettings = require('../main/model/GetSettingsResponseSettings');
const GetTranslationsRequest = require('../main/model/GetTranslationsRequest');
const GetTranslationsResponse = require('../main/model/GetTranslationsResponse');
const Language = require('../main/model/Language');
const NotifyUserRequest = require('../main/model/NotifyUserRequest');
const NotifyUserResponse = require('../main/model/NotifyUserResponse');
const PlayBeep = require('../main/model/PlayBeep');
const RecoverFileRequest = require('../main/model/RecoverFileRequest');
const RecoverFileResponse = require('../main/model/RecoverFileResponse');
const RegisterPhoneRequest = require('../main/model/RegisterPhoneRequest');
const RegisterPhoneResponse = require('../main/model/RegisterPhoneResponse');
const UpdateDeviceTokenRequest = require('../main/model/UpdateDeviceTokenRequest');
const UpdateDeviceTokenResponse = require('../main/model/UpdateDeviceTokenResponse');
const UpdateFileRequest = require('../main/model/UpdateFileRequest');
const UpdateFileResponse = require('../main/model/UpdateFileResponse');
const UpdateFolderRequest = require('../main/model/UpdateFolderRequest');
const UpdateFolderResponse = require('../main/model/UpdateFolderResponse');
const UpdateOrderRequest = require('../main/model/UpdateOrderRequest');
const UpdateOrderRequestFolder = require('../main/model/UpdateOrderRequestFolder');
const UpdateOrderResponse = require('../main/model/UpdateOrderResponse');
const UpdateProfileImgRequest = require('../main/model/UpdateProfileImgRequest');
const UpdateProfileImgResponse = require('../main/model/UpdateProfileImgResponse');
const UpdateProfileRequest = require('../main/model/UpdateProfileRequest');
const UpdateProfileRequestData = require('../main/model/UpdateProfileRequestData');
const UpdateProfileResponse = require('../main/model/UpdateProfileResponse');
const UpdateSettingsRequest = require('../main/model/UpdateSettingsRequest');
const UpdateSettingsResponse = require('../main/model/UpdateSettingsResponse');
const UpdateStarRequest = require('../main/model/UpdateStarRequest');
const UpdateStarResponse = require('../main/model/UpdateStarResponse');
const UpdateUserRequest = require('../main/model/UpdateUserRequest');
const UpdateUserResponse = require('../main/model/UpdateUserResponse');
const UploadMetaFileRequest = require('../main/model/UploadMetaFileRequest');
const UploadMetaFileResponse = require('../main/model/UploadMetaFileResponse');
const VerifyFolderPassRequest = require('../main/model/VerifyFolderPassRequest');
const VerifyFolderPassResponse = require('../main/model/VerifyFolderPassResponse');
const VerifyPhoneRequest = require('../main/model/VerifyPhoneRequest');
const VerifyPhoneResponse = require('../main/model/VerifyPhoneResponse');

const config = Configuration.builder()
    .baseUrl('https://app2.virtualbrix.net')
    .build();
const api = new CallRecorderApi(config);

provideCode = callback => {
	api.registerPhone(
		RegisterPhoneRequest.builder()
			.token('55942ee3894f51000530894')
			.phone('+16463742122')
			.build(),
		registerPhonePostResponse => {
			const registerPhonePostResponseCode = registerPhonePostResponse.code;
			const code = registerPhonePostResponseCode;
			callback(code);
		});
};

provideApiKey = callback => {
	api.registerPhone(
		RegisterPhoneRequest.builder()
			.token('55942ee3894f51000530894')
			.phone('+16463742122')
			.build(),
		registerPhonePostResponse => {
			const registerPhonePostResponseCode = registerPhonePostResponse.code;
			api.verifyPhone(
				VerifyPhoneRequest.builder()
					.token('55942ee3894f51000530894')
					.phone('+16463742122')
					.code(registerPhonePostResponseCode)
					.mcc('300')
					.app('rec')
					.deviceToken('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceId('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceType('ios')
					.timeZone('10')
					.build(),
				verifyPhonePostResponse => {
					const verifyPhonePostResponseApiKey = verifyPhonePostResponse.apiKey;
					const apiKey = verifyPhonePostResponseApiKey;
					callback(apiKey);
				});
		});
};

provideApiKeyAndFileId = callback => {
	api.registerPhone(
		RegisterPhoneRequest.builder()
			.token('55942ee3894f51000530894')
			.phone('+16463742122')
			.build(),
		registerPhonePostResponse => {
			const registerPhonePostResponseCode = registerPhonePostResponse.code;
			api.verifyPhone(
				VerifyPhoneRequest.builder()
					.token('55942ee3894f51000530894')
					.phone('+16463742122')
					.code(registerPhonePostResponseCode)
					.mcc('300')
					.app('rec')
					.deviceToken('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceId('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceType('ios')
					.timeZone('10')
					.build(),
				verifyPhonePostResponse => {
					const verifyPhonePostResponseApiKey = verifyPhonePostResponse.apiKey;
					api.createFile(
						CreateFileRequest.builder()
							.apiKey(verifyPhonePostResponseApiKey)
							.file('src/resources/audio.mp3')
							.data('')
							.build(),
						createFilePostResponse => {
							const createFilePostResponseId = createFilePostResponse.id;
							const apiKey = verifyPhonePostResponseApiKey;
							const fileId = createFilePostResponseId;
							callback(apiKey, fileId);
						});
				});
		});
};

provideApiKeyAndFolderId = callback => {
	api.registerPhone(
		RegisterPhoneRequest.builder()
			.token('55942ee3894f51000530894')
			.phone('+16463742122')
			.build(),
		registerPhonePostResponse => {
			const registerPhonePostResponseCode = registerPhonePostResponse.code;
			api.verifyPhone(
				VerifyPhoneRequest.builder()
					.token('55942ee3894f51000530894')
					.phone('+16463742122')
					.code(registerPhonePostResponseCode)
					.mcc('300')
					.app('rec')
					.deviceToken('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceId('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceType('ios')
					.timeZone('10')
					.build(),
				verifyPhonePostResponse => {
					const verifyPhonePostResponseApiKey = verifyPhonePostResponse.apiKey;
					api.createFolder(
						CreateFolderRequest.builder()
							.apiKey(verifyPhonePostResponseApiKey)
							.name('test-folder')
							.pass('12345')
							.build(),
						createFolderPostResponse => {
							const createFolderPostResponseId = createFolderPostResponse.id;
							const apiKey = verifyPhonePostResponseApiKey;
							const folderId = createFolderPostResponseId;
							callback(apiKey, folderId);
						});
				});
		});
};

provideApiKeyAndFileIdAndMetaFileId = callback => {
	api.registerPhone(
		RegisterPhoneRequest.builder()
			.token('55942ee3894f51000530894')
			.phone('+16463742122')
			.build(),
		registerPhonePostResponse => {
			const registerPhonePostResponseCode = registerPhonePostResponse.code;
			api.verifyPhone(
				VerifyPhoneRequest.builder()
					.token('55942ee3894f51000530894')
					.phone('+16463742122')
					.code(registerPhonePostResponseCode)
					.mcc('300')
					.app('rec')
					.deviceToken('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceId('871284c348e04a9cacab8aca6b2f3c9a')
					.deviceType('ios')
					.timeZone('10')
					.build(),
				verifyPhonePostResponse => {
					const verifyPhonePostResponseApiKey = verifyPhonePostResponse.apiKey;
					api.createFile(
						CreateFileRequest.builder()
							.apiKey(verifyPhonePostResponseApiKey)
							.file('src/resources/audio.mp3')
							.data('')
							.build(),
						createFilePostResponse => {
							const createFilePostResponseId = createFilePostResponse.id;
							api.uploadMetaFile(
								UploadMetaFileRequest.builder()
									.apiKey(verifyPhonePostResponseApiKey)
									.file('src/resources/java.png')
									.name('test-meta')
									.parentId(createFilePostResponseId)
									.id(1)
									.build(),
								uploadMetaFilePostResponse => {
									const uploadMetaFilePostResponseId = uploadMetaFilePostResponse.id;
									const apiKey = verifyPhonePostResponseApiKey;
									const fileId = createFilePostResponseId;
									const metaFileId = uploadMetaFilePostResponseId;
									callback(apiKey, fileId, metaFileId);
								});
						});
				});
		});
};



test('#buyCredits()', done => {
	provideApiKey(apiKey => {
		api.buyCredits(
			BuyCreditsRequest.builder()
				.apiKey(apiKey)
				.amount(100)
				.receipt('test')
				.productId(1)
				.deviceType('ios')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#cloneFile()', done => {
	provideApiKeyAndFileId((apiKey, fileId) => {
		api.cloneFile(
			CloneFileRequest.builder()
				.apiKey(apiKey)
				.id(fileId)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.code).toEqual('file_cloned');
				expect(response.id).toBeGreaterThan(0);
				done();
			});
	});
});


test('#createFile()', done => {
	provideApiKey(apiKey => {
		api.createFile(
			CreateFileRequest.builder()
				.apiKey(apiKey)
				.file('src/resources/audio.mp3')
				.data(CreateFileData.builder()
					.name('test-file')
					.email('e@mail.com')
					.phone('+16463742122')
					.lName('l')
					.fName('f')
					.notes('n')
					.tags('t')
					.source('s')
					.remindDays('10')
					.remindDate('2019-09-03T21:11:51.824121+03:00')
					.build())
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.id).toBeGreaterThan(0);
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#createFolder()', done => {
	provideApiKey(apiKey => {
		api.createFolder(
			CreateFolderRequest.builder()
				.apiKey(apiKey)
				.name('test-folder')
				.pass('12345')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.id).toBeGreaterThan(0);
				expect(response.code).toEqual('folder_created');
				done();
			});
	});
});


test('#deleteFiles()', done => {
	provideApiKeyAndFileId((apiKey, fileId) => {
		api.deleteFiles(
			DeleteFilesRequest.builder()
				.apiKey(apiKey)
				.ids([fileId])
				.action('remove_forever')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#deleteFolder()', done => {
	provideApiKeyAndFolderId((apiKey, folderId) => {
		api.deleteFolder(
			DeleteFolderRequest.builder()
				.apiKey(apiKey)
				.id(folderId)
				.moveTo('')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#deleteMetaFiles()', done => {
	provideApiKeyAndFileIdAndMetaFileId((apiKey, fileId, metaFileId) => {
		api.deleteMetaFiles(
			DeleteMetaFilesRequest.builder()
				.apiKey(apiKey)
				.ids([metaFileId])
				.parentId(fileId)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#getFiles()', done => {
	provideApiKey(apiKey => {
		api.getFiles(
			GetFilesRequest.builder()
				.apiKey(apiKey)
				.page(0)
				.folderId(0)
				.source('all')
				.pass('12345')
				.reminder(false)
				.q('hello')
				.id(0)
				.op('greater')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.credits).toEqual(0);
				expect(response.creditsTrans).toEqual(0);
				expect(response.files).toHaveLength(0);
				done();
			});
	});
});


test('#getFolders()', done => {
	provideApiKey(apiKey => {
		api.getFolders(
			GetFoldersRequest.builder()
				.apiKey(apiKey)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.folders).toBeTruthy();
				done();
			});
	});
});


test('#getLanguages()', done => {
	provideApiKey(apiKey => {
		api.getLanguages(
			GetLanguagesRequest.builder()
				.apiKey(apiKey)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.languages).toBeDefined();
				done();
			});
	});
});


test('#getMetaFiles()', done => {
	provideApiKeyAndFileId((apiKey, fileId) => {
		api.getMetaFiles(
			GetMetaFilesRequest.builder()
				.apiKey(apiKey)
				.parentId(fileId)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.metaFiles).toBeDefined();
				done();
			});
	});
});


test('#getMsgs()', done => {
	provideApiKey(apiKey => {
		api.getMsgs(
			GetMessagesRequest.builder()
				.apiKey(apiKey)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msgs).toBeDefined();
				done();
			});
	});
});


test('#getPhones()', done => {
	provideApiKey(apiKey => {
		api.getPhones(
			GetPhonesRequest.builder()
				.apiKey(apiKey)
				.build(),
			response => {
				expect(response).toBeDefined();
				done();
			});
	});
});


test('#getProfile()', done => {
	provideApiKey(apiKey => {
		api.getProfile(
			GetProfileRequest.builder()
				.apiKey(apiKey)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.code).toBeTruthy();
				expect(response.profile).toBeDefined();
				expect(response.app).toBeDefined();
				expect(response.shareUrl).toBeDefined();
				expect(response.rateUrl).toBeDefined();
				expect(response.credits).toEqual(0);
				expect(response.creditsTrans).toEqual(0);
				done();
			});
	});
});


test('#getSettings()', done => {
	provideApiKey(apiKey => {
		api.getSettings(
			GetSettingsRequest.builder()
				.apiKey(apiKey)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.app).toBeDefined();
				expect(response.credits).toEqual(0);
				expect(response.settings).toBeDefined();
				done();
			});
	});
});


test('#getTranslations()', done => {
	provideApiKey(apiKey => {
		api.getTranslations(
			GetTranslationsRequest.builder()
				.apiKey(apiKey)
				.language('en_US')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.translation).toBeDefined();
				done();
			});
	});
});


test('#notifyUserCustom()', done => {
	provideApiKey(apiKey => {
		api.notifyUserCustom(
			NotifyUserRequest.builder()
				.apiKey(apiKey)
				.title('test-title')
				.body('test-body')
				.deviceType('ios')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#recoverFile()', done => {
	provideApiKeyAndFileId((apiKey, fileId) => {
		api.recoverFile(
			RecoverFileRequest.builder()
				.apiKey(apiKey)
				.id(fileId)
				.folderId(0)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#registerPhone()', done => {
	api.registerPhone(
		RegisterPhoneRequest.builder()
			.token('55942ee3894f51000530894')
			.phone('+16463742122')
			.build(),
		response => {
			expect(response).toBeDefined();
			expect(response.status).toEqual('ok');
			expect(response.phone).toBeDefined();
			expect(response.code).toBeTruthy();
			expect(response.msg).toContain('Verification Code Sent');
			done();
		});
});


test('#updateDeviceToken()', done => {
	provideApiKey(apiKey => {
		api.updateDeviceToken(
			UpdateDeviceTokenRequest.builder()
				.apiKey(apiKey)
				.deviceToken('871284c348e04a9cacab8aca6b2f3c9a')
				.deviceType('ios')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#updateFile()', done => {
	provideApiKeyAndFileId((apiKey, fileId) => {
		api.updateFile(
			UpdateFileRequest.builder()
				.apiKey(apiKey)
				.id(fileId)
				.fName('f')
				.lName('l')
				.notes('n')
				.email('e@mail.ru')
				.phone('+16463742122')
				.tags('t')
				.folderId(0)
				.name('n')
				.remindDays('10')
				.remindDate('2019-09-03T21:11:51.824121+03:00')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				done();
			});
	});
});


test('#updateFolder()', done => {
	provideApiKeyAndFolderId((apiKey, folderId) => {
		api.updateFolder(
			UpdateFolderRequest.builder()
				.apiKey(apiKey)
				.id(folderId)
				.name('n')
				.pass('12345')
				.isPrivate(true)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg).toContain('Folder Updated');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#updateOrder()', done => {
	provideApiKey(apiKey => {
		api.updateOrder(
			UpdateOrderRequest.builder()
				.apiKey(apiKey)
				.folders([])
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg).toContain('Order Updated');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#updateProfileImg()', done => {
	provideApiKey(apiKey => {
		api.updateProfileImg(
			UpdateProfileImgRequest.builder()
				.apiKey(apiKey)
				.file('src/resources/java.png')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg).toContain('Profile Picture Updated');
				expect(response.code).toBeTruthy();
				expect(response.file).toBeDefined();
				expect(response.path).toBeDefined();
				done();
			});
	});
});


test('#updateProfile()', done => {
	provideApiKey(apiKey => {
		api.updateProfile(
			UpdateProfileRequest.builder()
				.apiKey(apiKey)
				.data(UpdateProfileRequestData.builder()
					.fName('f')
					.lName('l')
					.email('e@mail.ru')
					.isPublic('')
					.language('')
					.build())
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg).toContain('Profile Updated');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#updateSettings()', done => {
	provideApiKey(apiKey => {
		api.updateSettings(
			UpdateSettingsRequest.builder()
				.apiKey(apiKey)
				.playBeep('no')
				.filesPermission('private')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#updateStar()', done => {
	provideApiKeyAndFileId((apiKey, fileId) => {
		api.updateStar(
			UpdateStarRequest.builder()
				.apiKey(apiKey)
				.id(fileId)
				.star(true)
				.type('file')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#updateUser()', done => {
	provideApiKey(apiKey => {
		api.updateUser(
			UpdateUserRequest.builder()
				.apiKey(apiKey)
				.app('rec')
				.timeZone('10')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#uploadMetaFile()', done => {
	provideApiKeyAndFileId((apiKey, fileId) => {
		api.uploadMetaFile(
			UploadMetaFileRequest.builder()
				.apiKey(apiKey)
				.file('src/resources/java.png')
				.name('test-meta')
				.parentId(fileId)
				.id(1)
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg.toLowerCase()).toContain('success');
				expect(response.parentId).toEqual(fileId);
				expect(response.id).toBeGreaterThan(0);
				done();
			});
	});
});


test('#verifyFolderPass()', done => {
	provideApiKeyAndFolderId((apiKey, folderId) => {
		api.verifyFolderPass(
			VerifyFolderPassRequest.builder()
				.apiKey(apiKey)
				.id(folderId)
				.pass('12345')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.msg).toContain('Correct');
				expect(response.code).toBeTruthy();
				done();
			});
	});
});


test('#verifyPhone()', done => {
	provideCode(code => {
		api.verifyPhone(
			VerifyPhoneRequest.builder()
				.token('55942ee3894f51000530894')
				.phone('+16463742122')
				.code(code)
				.mcc('300')
				.app('rec')
				.deviceToken('871284c348e04a9cacab8aca6b2f3c9a')
				.deviceId('871284c348e04a9cacab8aca6b2f3c9a')
				.deviceType('ios')
				.timeZone('10')
				.build(),
			response => {
				expect(response).toBeDefined();
				expect(response.status).toEqual('ok');
				expect(response.phone).toBeDefined();
				expect(response.apiKey).toBeDefined();
				expect(response.msg).toContain('Phone Verified');
				done();
			});
	});
});

