class DeleteFolderRequest {
    constructor(
        apiKey,
        id,
        moveTo
    ) {
        this.apiKey = apiKey;
        this.id = id;
        this.moveTo = moveTo;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         moveTo(moveTo) {
            this._moveTo = moveTo;
            return this;
         }
         build() {
            return new DeleteFolderRequest(
                this._apiKey,
                this._id,
                this._moveTo
            );
         }
      }
      return new Builder();
   }
}

module.exports = DeleteFolderRequest;

