class CloneFileRequest {
    constructor(
        apiKey,
        id
    ) {
        this.apiKey = apiKey;
        this.id = id;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         build() {
            return new CloneFileRequest(
                this._apiKey,
                this._id
            );
         }
      }
      return new Builder();
   }
}

module.exports = CloneFileRequest;

