class UpdateProfileImgResponse {
    constructor(
        status,
        msg,
        code,
        file,
        path
    ) {
        this.status = status;
        this.msg = msg;
        this.code = code;
        this.file = file;
        this.path = path;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         file(file) {
            this._file = file;
            return this;
         }
         path(path) {
            this._path = path;
            return this;
         }
         build() {
            return new UpdateProfileImgResponse(
                this._status,
                this._msg,
                this._code,
                this._file,
                this._path
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateProfileImgResponse;

