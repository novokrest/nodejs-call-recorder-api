class UpdateDeviceTokenRequest {
    constructor(
        apiKey,
        deviceToken,
        deviceType
    ) {
        this.apiKey = apiKey;
        this.deviceToken = deviceToken;
        this.deviceType = deviceType;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         deviceToken(deviceToken) {
            this._deviceToken = deviceToken;
            return this;
         }
         deviceType(deviceType) {
            this._deviceType = deviceType;
            return this;
         }
         build() {
            return new UpdateDeviceTokenRequest(
                this._apiKey,
                this._deviceToken,
                this._deviceType
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateDeviceTokenRequest;

