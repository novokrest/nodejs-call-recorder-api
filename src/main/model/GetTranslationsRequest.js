class GetTranslationsRequest {
    constructor(
        apiKey,
        language
    ) {
        this.apiKey = apiKey;
        this.language = language;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         language(language) {
            this._language = language;
            return this;
         }
         build() {
            return new GetTranslationsRequest(
                this._apiKey,
                this._language
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetTranslationsRequest;

