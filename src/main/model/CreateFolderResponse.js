class CreateFolderResponse {
    constructor(
        status,
        msg,
        id,
        code
    ) {
        this.status = status;
        this.msg = msg;
        this.id = id;
        this.code = code;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         build() {
            return new CreateFolderResponse(
                this._status,
                this._msg,
                this._id,
                this._code
            );
         }
      }
      return new Builder();
   }
}

module.exports = CreateFolderResponse;

