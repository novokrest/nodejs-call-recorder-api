class RegisterPhoneRequest {
    constructor(
        token,
        phone
    ) {
        this.token = token;
        this.phone = phone;
    }

   static builder() {
      class Builder {
         token(token) {
            this._token = token;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         build() {
            return new RegisterPhoneRequest(
                this._token,
                this._phone
            );
         }
      }
      return new Builder();
   }
}

module.exports = RegisterPhoneRequest;

