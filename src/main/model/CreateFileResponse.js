class CreateFileResponse {
    constructor(
        status,
        id,
        msg
    ) {
        this.status = status;
        this.id = id;
        this.msg = msg;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         build() {
            return new CreateFileResponse(
                this._status,
                this._id,
                this._msg
            );
         }
      }
      return new Builder();
   }
}

module.exports = CreateFileResponse;

