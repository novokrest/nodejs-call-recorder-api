class Language {
    constructor(
        code,
        name,
        flag
    ) {
        this.code = code;
        this.name = name;
        this.flag = flag;
    }

   static builder() {
      class Builder {
         code(code) {
            this._code = code;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         flag(flag) {
            this._flag = flag;
            return this;
         }
         build() {
            return new Language(
                this._code,
                this._name,
                this._flag
            );
         }
      }
      return new Builder();
   }
}

module.exports = Language;

