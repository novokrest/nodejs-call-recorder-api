class BuyCreditsRequest {
    constructor(
        apiKey,
        amount,
        receipt,
        productId,
        deviceType
    ) {
        this.apiKey = apiKey;
        this.amount = amount;
        this.receipt = receipt;
        this.productId = productId;
        this.deviceType = deviceType;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         amount(amount) {
            this._amount = amount;
            return this;
         }
         receipt(receipt) {
            this._receipt = receipt;
            return this;
         }
         productId(productId) {
            this._productId = productId;
            return this;
         }
         deviceType(deviceType) {
            this._deviceType = deviceType;
            return this;
         }
         build() {
            return new BuyCreditsRequest(
                this._apiKey,
                this._amount,
                this._receipt,
                this._productId,
                this._deviceType
            );
         }
      }
      return new Builder();
   }
}

module.exports = BuyCreditsRequest;

