class UpdateFileRequest {
    constructor(
        apiKey,
        id,
        fName,
        lName,
        notes,
        email,
        phone,
        tags,
        folderId,
        name,
        remindDays,
        remindDate
    ) {
        this.apiKey = apiKey;
        this.id = id;
        this.fName = fName;
        this.lName = lName;
        this.notes = notes;
        this.email = email;
        this.phone = phone;
        this.tags = tags;
        this.folderId = folderId;
        this.name = name;
        this.remindDays = remindDays;
        this.remindDate = remindDate;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         fName(fName) {
            this._fName = fName;
            return this;
         }
         lName(lName) {
            this._lName = lName;
            return this;
         }
         notes(notes) {
            this._notes = notes;
            return this;
         }
         email(email) {
            this._email = email;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         tags(tags) {
            this._tags = tags;
            return this;
         }
         folderId(folderId) {
            this._folderId = folderId;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         remindDays(remindDays) {
            this._remindDays = remindDays;
            return this;
         }
         remindDate(remindDate) {
            this._remindDate = remindDate;
            return this;
         }
         build() {
            return new UpdateFileRequest(
                this._apiKey,
                this._id,
                this._fName,
                this._lName,
                this._notes,
                this._email,
                this._phone,
                this._tags,
                this._folderId,
                this._name,
                this._remindDays,
                this._remindDate
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateFileRequest;

