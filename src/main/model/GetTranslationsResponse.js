class GetTranslationsResponse {
    constructor(
        status,
        msg,
        code,
        translation
    ) {
        this.status = status;
        this.msg = msg;
        this.code = code;
        this.translation = translation;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         translation(translation) {
            this._translation = translation;
            return this;
         }
         build() {
            return new GetTranslationsResponse(
                this._status,
                this._msg,
                this._code,
                this._translation
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetTranslationsResponse;

