class GetPhonesResponsePhone {
    constructor(
        phoneNumber,
        number,
        prefix,
        friendlyName,
        flag,
        city,
        country
    ) {
        this.phoneNumber = phoneNumber;
        this.number = number;
        this.prefix = prefix;
        this.friendlyName = friendlyName;
        this.flag = flag;
        this.city = city;
        this.country = country;
    }

   static builder() {
      class Builder {
         phoneNumber(phoneNumber) {
            this._phoneNumber = phoneNumber;
            return this;
         }
         number(number) {
            this._number = number;
            return this;
         }
         prefix(prefix) {
            this._prefix = prefix;
            return this;
         }
         friendlyName(friendlyName) {
            this._friendlyName = friendlyName;
            return this;
         }
         flag(flag) {
            this._flag = flag;
            return this;
         }
         city(city) {
            this._city = city;
            return this;
         }
         country(country) {
            this._country = country;
            return this;
         }
         build() {
            return new GetPhonesResponsePhone(
                this._phoneNumber,
                this._number,
                this._prefix,
                this._friendlyName,
                this._flag,
                this._city,
                this._country
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetPhonesResponsePhone;

