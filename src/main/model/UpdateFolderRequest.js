class UpdateFolderRequest {
    constructor(
        apiKey,
        id,
        name,
        pass,
        isPrivate
    ) {
        this.apiKey = apiKey;
        this.id = id;
        this.name = name;
        this.pass = pass;
        this.isPrivate = isPrivate;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         pass(pass) {
            this._pass = pass;
            return this;
         }
         isPrivate(isPrivate) {
            this._isPrivate = isPrivate;
            return this;
         }
         build() {
            return new UpdateFolderRequest(
                this._apiKey,
                this._id,
                this._name,
                this._pass,
                this._isPrivate
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateFolderRequest;

