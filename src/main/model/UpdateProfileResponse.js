class UpdateProfileResponse {
    constructor(
        status,
        msg,
        code
    ) {
        this.status = status;
        this.msg = msg;
        this.code = code;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         build() {
            return new UpdateProfileResponse(
                this._status,
                this._msg,
                this._code
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateProfileResponse;

