class UpdateOrderRequestFolder {
    constructor(
        id,
        orderId
    ) {
        this.id = id;
        this.orderId = orderId;
    }

   static builder() {
      class Builder {
         id(id) {
            this._id = id;
            return this;
         }
         orderId(orderId) {
            this._orderId = orderId;
            return this;
         }
         build() {
            return new UpdateOrderRequestFolder(
                this._id,
                this._orderId
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateOrderRequestFolder;

