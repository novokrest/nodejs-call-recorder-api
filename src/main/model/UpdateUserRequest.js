class UpdateUserRequest {
    constructor(
        apiKey,
        app,
        timeZone
    ) {
        this.apiKey = apiKey;
        this.app = app;
        this.timeZone = timeZone;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         app(app) {
            this._app = app;
            return this;
         }
         timeZone(timeZone) {
            this._timeZone = timeZone;
            return this;
         }
         build() {
            return new UpdateUserRequest(
                this._apiKey,
                this._app,
                this._timeZone
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateUserRequest;

