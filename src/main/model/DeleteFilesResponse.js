class DeleteFilesResponse {
    constructor(
        status,
        msg
    ) {
        this.status = status;
        this.msg = msg;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         build() {
            return new DeleteFilesResponse(
                this._status,
                this._msg
            );
         }
      }
      return new Builder();
   }
}

module.exports = DeleteFilesResponse;

