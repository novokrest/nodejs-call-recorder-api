class GetPhonesRequest {
    constructor(
        apiKey
    ) {
        this.apiKey = apiKey;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         build() {
            return new GetPhonesRequest(
                this._apiKey
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetPhonesRequest;

