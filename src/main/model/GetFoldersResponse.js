class GetFoldersResponse {
    constructor(
        status,
        msg,
        folders
    ) {
        this.status = status;
        this.msg = msg;
        this.folders = folders;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         folders(folders) {
            this._folders = folders;
            return this;
         }
         build() {
            return new GetFoldersResponse(
                this._status,
                this._msg,
                this._folders
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetFoldersResponse;

