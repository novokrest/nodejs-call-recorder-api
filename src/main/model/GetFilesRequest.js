class GetFilesRequest {
    constructor(
        apiKey,
        page,
        folderId,
        source,
        pass,
        reminder,
        q,
        id,
        op
    ) {
        this.apiKey = apiKey;
        this.page = page;
        this.folderId = folderId;
        this.source = source;
        this.pass = pass;
        this.reminder = reminder;
        this.q = q;
        this.id = id;
        this.op = op;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         page(page) {
            this._page = page;
            return this;
         }
         folderId(folderId) {
            this._folderId = folderId;
            return this;
         }
         source(source) {
            this._source = source;
            return this;
         }
         pass(pass) {
            this._pass = pass;
            return this;
         }
         reminder(reminder) {
            this._reminder = reminder;
            return this;
         }
         q(q) {
            this._q = q;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         op(op) {
            this._op = op;
            return this;
         }
         build() {
            return new GetFilesRequest(
                this._apiKey,
                this._page,
                this._folderId,
                this._source,
                this._pass,
                this._reminder,
                this._q,
                this._id,
                this._op
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetFilesRequest;

