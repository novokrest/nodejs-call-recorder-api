class UpdateProfileRequestData {
    constructor(
        fName,
        lName,
        email,
        isPublic,
        language
    ) {
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.isPublic = isPublic;
        this.language = language;
    }

   static builder() {
      class Builder {
         fName(fName) {
            this._fName = fName;
            return this;
         }
         lName(lName) {
            this._lName = lName;
            return this;
         }
         email(email) {
            this._email = email;
            return this;
         }
         isPublic(isPublic) {
            this._isPublic = isPublic;
            return this;
         }
         language(language) {
            this._language = language;
            return this;
         }
         build() {
            return new UpdateProfileRequestData(
                this._fName,
                this._lName,
                this._email,
                this._isPublic,
                this._language
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateProfileRequestData;

