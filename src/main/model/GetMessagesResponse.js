class GetMessagesResponse {
    constructor(
        status,
        msgs
    ) {
        this.status = status;
        this.msgs = msgs;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msgs(msgs) {
            this._msgs = msgs;
            return this;
         }
         build() {
            return new GetMessagesResponse(
                this._status,
                this._msgs
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetMessagesResponse;

