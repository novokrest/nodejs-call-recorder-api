class GetLanguagesResponse {
    constructor(
        status,
        msg,
        languages
    ) {
        this.status = status;
        this.msg = msg;
        this.languages = languages;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         languages(languages) {
            this._languages = languages;
            return this;
         }
         build() {
            return new GetLanguagesResponse(
                this._status,
                this._msg,
                this._languages
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetLanguagesResponse;

