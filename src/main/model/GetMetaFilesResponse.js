class GetMetaFilesResponse {
    constructor(
        status,
        metaFiles
    ) {
        this.status = status;
        this.metaFiles = metaFiles;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         metaFiles(metaFiles) {
            this._metaFiles = metaFiles;
            return this;
         }
         build() {
            return new GetMetaFilesResponse(
                this._status,
                this._metaFiles
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetMetaFilesResponse;

