class GetProfileResponse {
    constructor(
        status,
        code,
        profile,
        app,
        shareUrl,
        rateUrl,
        credits,
        creditsTrans
    ) {
        this.status = status;
        this.code = code;
        this.profile = profile;
        this.app = app;
        this.shareUrl = shareUrl;
        this.rateUrl = rateUrl;
        this.credits = credits;
        this.creditsTrans = creditsTrans;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         profile(profile) {
            this._profile = profile;
            return this;
         }
         app(app) {
            this._app = app;
            return this;
         }
         shareUrl(shareUrl) {
            this._shareUrl = shareUrl;
            return this;
         }
         rateUrl(rateUrl) {
            this._rateUrl = rateUrl;
            return this;
         }
         credits(credits) {
            this._credits = credits;
            return this;
         }
         creditsTrans(creditsTrans) {
            this._creditsTrans = creditsTrans;
            return this;
         }
         build() {
            return new GetProfileResponse(
                this._status,
                this._code,
                this._profile,
                this._app,
                this._shareUrl,
                this._rateUrl,
                this._credits,
                this._creditsTrans
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetProfileResponse;

