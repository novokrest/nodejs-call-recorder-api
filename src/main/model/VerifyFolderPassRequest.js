class VerifyFolderPassRequest {
    constructor(
        apiKey,
        id,
        pass
    ) {
        this.apiKey = apiKey;
        this.id = id;
        this.pass = pass;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         pass(pass) {
            this._pass = pass;
            return this;
         }
         build() {
            return new VerifyFolderPassRequest(
                this._apiKey,
                this._id,
                this._pass
            );
         }
      }
      return new Builder();
   }
}

module.exports = VerifyFolderPassRequest;

