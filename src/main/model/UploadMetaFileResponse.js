class UploadMetaFileResponse {
    constructor(
        status,
        msg,
        parentId,
        id
    ) {
        this.status = status;
        this.msg = msg;
        this.parentId = parentId;
        this.id = id;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         parentId(parentId) {
            this._parentId = parentId;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         build() {
            return new UploadMetaFileResponse(
                this._status,
                this._msg,
                this._parentId,
                this._id
            );
         }
      }
      return new Builder();
   }
}

module.exports = UploadMetaFileResponse;

