class CloneFileResponse {
    constructor(
        status,
        msg,
        code,
        id
    ) {
        this.status = status;
        this.msg = msg;
        this.code = code;
        this.id = id;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         build() {
            return new CloneFileResponse(
                this._status,
                this._msg,
                this._code,
                this._id
            );
         }
      }
      return new Builder();
   }
}

module.exports = CloneFileResponse;

