class DeleteFilesRequest {
    constructor(
        apiKey,
        ids,
        action
    ) {
        this.apiKey = apiKey;
        this.ids = ids;
        this.action = action;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         ids(ids) {
            this._ids = ids;
            return this;
         }
         action(action) {
            this._action = action;
            return this;
         }
         build() {
            return new DeleteFilesRequest(
                this._apiKey,
                this._ids,
                this._action
            );
         }
      }
      return new Builder();
   }
}

module.exports = DeleteFilesRequest;

