class CreateFolderRequest {
    constructor(
        apiKey,
        name,
        pass
    ) {
        this.apiKey = apiKey;
        this.name = name;
        this.pass = pass;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         pass(pass) {
            this._pass = pass;
            return this;
         }
         build() {
            return new CreateFolderRequest(
                this._apiKey,
                this._name,
                this._pass
            );
         }
      }
      return new Builder();
   }
}

module.exports = CreateFolderRequest;

