class VerifyPhoneResponse {
    constructor(
        status,
        phone,
        apiKey,
        msg
    ) {
        this.status = status;
        this.phone = phone;
        this.apiKey = apiKey;
        this.msg = msg;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         build() {
            return new VerifyPhoneResponse(
                this._status,
                this._phone,
                this._apiKey,
                this._msg
            );
         }
      }
      return new Builder();
   }
}

module.exports = VerifyPhoneResponse;

