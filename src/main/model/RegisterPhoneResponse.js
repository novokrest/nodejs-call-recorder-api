class RegisterPhoneResponse {
    constructor(
        status,
        phone,
        code,
        msg
    ) {
        this.status = status;
        this.phone = phone;
        this.code = code;
        this.msg = msg;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         msg(msg) {
            this._msg = msg;
            return this;
         }
         build() {
            return new RegisterPhoneResponse(
                this._status,
                this._phone,
                this._code,
                this._msg
            );
         }
      }
      return new Builder();
   }
}

module.exports = RegisterPhoneResponse;

