class App {
    constructor(
    ) {
    }

   static builder() {
      class Builder {
         build() {
            return new App(
            );
         }
      }
      return new Builder();
   }
}

module.exports = App;

