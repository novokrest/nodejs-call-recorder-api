class GetFilesResponseFile {
    constructor(
        id,
        accessNumber,
        name,
        fName,
        lName,
        email,
        phone,
        notes,
        meta,
        source,
        url,
        credits,
        duration,
        time,
        shareUrl,
        downloadUrl
    ) {
        this.id = id;
        this.accessNumber = accessNumber;
        this.name = name;
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.phone = phone;
        this.notes = notes;
        this.meta = meta;
        this.source = source;
        this.url = url;
        this.credits = credits;
        this.duration = duration;
        this.time = time;
        this.shareUrl = shareUrl;
        this.downloadUrl = downloadUrl;
    }

   static builder() {
      class Builder {
         id(id) {
            this._id = id;
            return this;
         }
         accessNumber(accessNumber) {
            this._accessNumber = accessNumber;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         fName(fName) {
            this._fName = fName;
            return this;
         }
         lName(lName) {
            this._lName = lName;
            return this;
         }
         email(email) {
            this._email = email;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         notes(notes) {
            this._notes = notes;
            return this;
         }
         meta(meta) {
            this._meta = meta;
            return this;
         }
         source(source) {
            this._source = source;
            return this;
         }
         url(url) {
            this._url = url;
            return this;
         }
         credits(credits) {
            this._credits = credits;
            return this;
         }
         duration(duration) {
            this._duration = duration;
            return this;
         }
         time(time) {
            this._time = time;
            return this;
         }
         shareUrl(shareUrl) {
            this._shareUrl = shareUrl;
            return this;
         }
         downloadUrl(downloadUrl) {
            this._downloadUrl = downloadUrl;
            return this;
         }
         build() {
            return new GetFilesResponseFile(
                this._id,
                this._accessNumber,
                this._name,
                this._fName,
                this._lName,
                this._email,
                this._phone,
                this._notes,
                this._meta,
                this._source,
                this._url,
                this._credits,
                this._duration,
                this._time,
                this._shareUrl,
                this._downloadUrl
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetFilesResponseFile;

