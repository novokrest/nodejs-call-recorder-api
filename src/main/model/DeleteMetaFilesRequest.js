class DeleteMetaFilesRequest {
    constructor(
        apiKey,
        ids,
        parentId
    ) {
        this.apiKey = apiKey;
        this.ids = ids;
        this.parentId = parentId;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         ids(ids) {
            this._ids = ids;
            return this;
         }
         parentId(parentId) {
            this._parentId = parentId;
            return this;
         }
         build() {
            return new DeleteMetaFilesRequest(
                this._apiKey,
                this._ids,
                this._parentId
            );
         }
      }
      return new Builder();
   }
}

module.exports = DeleteMetaFilesRequest;

