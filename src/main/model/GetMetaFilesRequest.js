class GetMetaFilesRequest {
    constructor(
        apiKey,
        parentId
    ) {
        this.apiKey = apiKey;
        this.parentId = parentId;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         parentId(parentId) {
            this._parentId = parentId;
            return this;
         }
         build() {
            return new GetMetaFilesRequest(
                this._apiKey,
                this._parentId
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetMetaFilesRequest;

