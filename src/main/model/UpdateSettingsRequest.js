class UpdateSettingsRequest {
    constructor(
        apiKey,
        playBeep,
        filesPermission
    ) {
        this.apiKey = apiKey;
        this.playBeep = playBeep;
        this.filesPermission = filesPermission;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         playBeep(playBeep) {
            this._playBeep = playBeep;
            return this;
         }
         filesPermission(filesPermission) {
            this._filesPermission = filesPermission;
            return this;
         }
         build() {
            return new UpdateSettingsRequest(
                this._apiKey,
                this._playBeep,
                this._filesPermission
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateSettingsRequest;

