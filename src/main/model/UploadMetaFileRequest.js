class UploadMetaFileRequest {
    constructor(
        apiKey,
        file,
        name,
        parentId,
        id
    ) {
        this.apiKey = apiKey;
        this.file = file;
        this.name = name;
        this.parentId = parentId;
        this.id = id;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         file(file) {
            this._file = file;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         parentId(parentId) {
            this._parentId = parentId;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         build() {
            return new UploadMetaFileRequest(
                this._apiKey,
                this._file,
                this._name,
                this._parentId,
                this._id
            );
         }
      }
      return new Builder();
   }
}

module.exports = UploadMetaFileRequest;

