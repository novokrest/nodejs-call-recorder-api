class UpdateProfileRequest {
    constructor(
        apiKey,
        data
    ) {
        this.apiKey = apiKey;
        this.data = data;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         data(data) {
            this._data = data;
            return this;
         }
         build() {
            return new UpdateProfileRequest(
                this._apiKey,
                this._data
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateProfileRequest;

