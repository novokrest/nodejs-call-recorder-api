class NotifyUserRequest {
    constructor(
        apiKey,
        title,
        body,
        deviceType
    ) {
        this.apiKey = apiKey;
        this.title = title;
        this.body = body;
        this.deviceType = deviceType;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         title(title) {
            this._title = title;
            return this;
         }
         body(body) {
            this._body = body;
            return this;
         }
         deviceType(deviceType) {
            this._deviceType = deviceType;
            return this;
         }
         build() {
            return new NotifyUserRequest(
                this._apiKey,
                this._title,
                this._body,
                this._deviceType
            );
         }
      }
      return new Builder();
   }
}

module.exports = NotifyUserRequest;

