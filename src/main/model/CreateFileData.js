class CreateFileData {
    constructor(
        name,
        email,
        phone,
        lName,
        fName,
        notes,
        tags,
        source,
        remindDays,
        remindDate
    ) {
        this.name = name;
        this.email = email;
        this.phone = phone;
        this.lName = lName;
        this.fName = fName;
        this.notes = notes;
        this.tags = tags;
        this.source = source;
        this.remindDays = remindDays;
        this.remindDate = remindDate;
    }

   static builder() {
      class Builder {
         name(name) {
            this._name = name;
            return this;
         }
         email(email) {
            this._email = email;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         lName(lName) {
            this._lName = lName;
            return this;
         }
         fName(fName) {
            this._fName = fName;
            return this;
         }
         notes(notes) {
            this._notes = notes;
            return this;
         }
         tags(tags) {
            this._tags = tags;
            return this;
         }
         source(source) {
            this._source = source;
            return this;
         }
         remindDays(remindDays) {
            this._remindDays = remindDays;
            return this;
         }
         remindDate(remindDate) {
            this._remindDate = remindDate;
            return this;
         }
         build() {
            return new CreateFileData(
                this._name,
                this._email,
                this._phone,
                this._lName,
                this._fName,
                this._notes,
                this._tags,
                this._source,
                this._remindDays,
                this._remindDate
            );
         }
      }
      return new Builder();
   }
}

module.exports = CreateFileData;

