class UpdateStarRequest {
    constructor(
        apiKey,
        id,
        star,
        type
    ) {
        this.apiKey = apiKey;
        this.id = id;
        this.star = star;
        this.type = type;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         star(star) {
            this._star = star;
            return this;
         }
         type(type) {
            this._type = type;
            return this;
         }
         build() {
            return new UpdateStarRequest(
                this._apiKey,
                this._id,
                this._star,
                this._type
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateStarRequest;

