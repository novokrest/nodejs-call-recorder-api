class GetFilesResponse {
    constructor(
        status,
        credits,
        creditsTrans,
        files
    ) {
        this.status = status;
        this.credits = credits;
        this.creditsTrans = creditsTrans;
        this.files = files;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         credits(credits) {
            this._credits = credits;
            return this;
         }
         creditsTrans(creditsTrans) {
            this._creditsTrans = creditsTrans;
            return this;
         }
         files(files) {
            this._files = files;
            return this;
         }
         build() {
            return new GetFilesResponse(
                this._status,
                this._credits,
                this._creditsTrans,
                this._files
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetFilesResponse;

