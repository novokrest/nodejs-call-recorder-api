class GetMessagesResponseMsg {
    constructor(
        id,
        title,
        body,
        time
    ) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.time = time;
    }

   static builder() {
      class Builder {
         id(id) {
            this._id = id;
            return this;
         }
         title(title) {
            this._title = title;
            return this;
         }
         body(body) {
            this._body = body;
            return this;
         }
         time(time) {
            this._time = time;
            return this;
         }
         build() {
            return new GetMessagesResponseMsg(
                this._id,
                this._title,
                this._body,
                this._time
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetMessagesResponseMsg;

