class GetSettingsResponseSettings {
    constructor(
        playBeep,
        filesPermission
    ) {
        this.playBeep = playBeep;
        this.filesPermission = filesPermission;
    }

   static builder() {
      class Builder {
         playBeep(playBeep) {
            this._playBeep = playBeep;
            return this;
         }
         filesPermission(filesPermission) {
            this._filesPermission = filesPermission;
            return this;
         }
         build() {
            return new GetSettingsResponseSettings(
                this._playBeep,
                this._filesPermission
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetSettingsResponseSettings;

