class UpdateProfileImgRequest {
    constructor(
        apiKey,
        file
    ) {
        this.apiKey = apiKey;
        this.file = file;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         file(file) {
            this._file = file;
            return this;
         }
         build() {
            return new UpdateProfileImgRequest(
                this._apiKey,
                this._file
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateProfileImgRequest;

