class UpdateOrderRequest {
    constructor(
        apiKey,
        folders
    ) {
        this.apiKey = apiKey;
        this.folders = folders;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         folders(folders) {
            this._folders = folders;
            return this;
         }
         build() {
            return new UpdateOrderRequest(
                this._apiKey,
                this._folders
            );
         }
      }
      return new Builder();
   }
}

module.exports = UpdateOrderRequest;

