class RecoverFileRequest {
    constructor(
        apiKey,
        id,
        folderId
    ) {
        this.apiKey = apiKey;
        this.id = id;
        this.folderId = folderId;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         id(id) {
            this._id = id;
            return this;
         }
         folderId(folderId) {
            this._folderId = folderId;
            return this;
         }
         build() {
            return new RecoverFileRequest(
                this._apiKey,
                this._id,
                this._folderId
            );
         }
      }
      return new Builder();
   }
}

module.exports = RecoverFileRequest;

