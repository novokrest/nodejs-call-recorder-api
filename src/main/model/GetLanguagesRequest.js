class GetLanguagesRequest {
    constructor(
        apiKey
    ) {
        this.apiKey = apiKey;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         build() {
            return new GetLanguagesRequest(
                this._apiKey
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetLanguagesRequest;

