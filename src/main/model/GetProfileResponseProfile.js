class GetProfileResponseProfile {
    constructor(
        fName,
        lName,
        email,
        phone,
        pic,
        language,
        isPublic,
        playBeep,
        maxLength,
        timeZone,
        time,
        pin
    ) {
        this.fName = fName;
        this.lName = lName;
        this.email = email;
        this.phone = phone;
        this.pic = pic;
        this.language = language;
        this.isPublic = isPublic;
        this.playBeep = playBeep;
        this.maxLength = maxLength;
        this.timeZone = timeZone;
        this.time = time;
        this.pin = pin;
    }

   static builder() {
      class Builder {
         fName(fName) {
            this._fName = fName;
            return this;
         }
         lName(lName) {
            this._lName = lName;
            return this;
         }
         email(email) {
            this._email = email;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         pic(pic) {
            this._pic = pic;
            return this;
         }
         language(language) {
            this._language = language;
            return this;
         }
         isPublic(isPublic) {
            this._isPublic = isPublic;
            return this;
         }
         playBeep(playBeep) {
            this._playBeep = playBeep;
            return this;
         }
         maxLength(maxLength) {
            this._maxLength = maxLength;
            return this;
         }
         timeZone(timeZone) {
            this._timeZone = timeZone;
            return this;
         }
         time(time) {
            this._time = time;
            return this;
         }
         pin(pin) {
            this._pin = pin;
            return this;
         }
         build() {
            return new GetProfileResponseProfile(
                this._fName,
                this._lName,
                this._email,
                this._phone,
                this._pic,
                this._language,
                this._isPublic,
                this._playBeep,
                this._maxLength,
                this._timeZone,
                this._time,
                this._pin
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetProfileResponseProfile;

