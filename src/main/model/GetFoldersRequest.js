class GetFoldersRequest {
    constructor(
        apiKey
    ) {
        this.apiKey = apiKey;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         build() {
            return new GetFoldersRequest(
                this._apiKey
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetFoldersRequest;

