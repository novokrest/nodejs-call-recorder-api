class GetSettingsResponse {
    constructor(
        status,
        app,
        credits,
        settings
    ) {
        this.status = status;
        this.app = app;
        this.credits = credits;
        this.settings = settings;
    }

   static builder() {
      class Builder {
         status(status) {
            this._status = status;
            return this;
         }
         app(app) {
            this._app = app;
            return this;
         }
         credits(credits) {
            this._credits = credits;
            return this;
         }
         settings(settings) {
            this._settings = settings;
            return this;
         }
         build() {
            return new GetSettingsResponse(
                this._status,
                this._app,
                this._credits,
                this._settings
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetSettingsResponse;

