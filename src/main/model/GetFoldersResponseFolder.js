class GetFoldersResponseFolder {
    constructor(
        id,
        name,
        created,
        updated,
        isStart,
        orderId
    ) {
        this.id = id;
        this.name = name;
        this.created = created;
        this.updated = updated;
        this.isStart = isStart;
        this.orderId = orderId;
    }

   static builder() {
      class Builder {
         id(id) {
            this._id = id;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         created(created) {
            this._created = created;
            return this;
         }
         updated(updated) {
            this._updated = updated;
            return this;
         }
         isStart(isStart) {
            this._isStart = isStart;
            return this;
         }
         orderId(orderId) {
            this._orderId = orderId;
            return this;
         }
         build() {
            return new GetFoldersResponseFolder(
                this._id,
                this._name,
                this._created,
                this._updated,
                this._isStart,
                this._orderId
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetFoldersResponseFolder;

