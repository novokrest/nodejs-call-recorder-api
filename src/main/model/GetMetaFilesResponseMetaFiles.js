class GetMetaFilesResponseMetaFiles {
    constructor(
        id,
        parentId,
        name,
        file,
        userId,
        time
    ) {
        this.id = id;
        this.parentId = parentId;
        this.name = name;
        this.file = file;
        this.userId = userId;
        this.time = time;
    }

   static builder() {
      class Builder {
         id(id) {
            this._id = id;
            return this;
         }
         parentId(parentId) {
            this._parentId = parentId;
            return this;
         }
         name(name) {
            this._name = name;
            return this;
         }
         file(file) {
            this._file = file;
            return this;
         }
         userId(userId) {
            this._userId = userId;
            return this;
         }
         time(time) {
            this._time = time;
            return this;
         }
         build() {
            return new GetMetaFilesResponseMetaFiles(
                this._id,
                this._parentId,
                this._name,
                this._file,
                this._userId,
                this._time
            );
         }
      }
      return new Builder();
   }
}

module.exports = GetMetaFilesResponseMetaFiles;

