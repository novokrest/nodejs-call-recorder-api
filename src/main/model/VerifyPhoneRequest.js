class VerifyPhoneRequest {
    constructor(
        token,
        phone,
        code,
        mcc,
        app,
        deviceToken,
        deviceId,
        deviceType,
        timeZone
    ) {
        this.token = token;
        this.phone = phone;
        this.code = code;
        this.mcc = mcc;
        this.app = app;
        this.deviceToken = deviceToken;
        this.deviceId = deviceId;
        this.deviceType = deviceType;
        this.timeZone = timeZone;
    }

   static builder() {
      class Builder {
         token(token) {
            this._token = token;
            return this;
         }
         phone(phone) {
            this._phone = phone;
            return this;
         }
         code(code) {
            this._code = code;
            return this;
         }
         mcc(mcc) {
            this._mcc = mcc;
            return this;
         }
         app(app) {
            this._app = app;
            return this;
         }
         deviceToken(deviceToken) {
            this._deviceToken = deviceToken;
            return this;
         }
         deviceId(deviceId) {
            this._deviceId = deviceId;
            return this;
         }
         deviceType(deviceType) {
            this._deviceType = deviceType;
            return this;
         }
         timeZone(timeZone) {
            this._timeZone = timeZone;
            return this;
         }
         build() {
            return new VerifyPhoneRequest(
                this._token,
                this._phone,
                this._code,
                this._mcc,
                this._app,
                this._deviceToken,
                this._deviceId,
                this._deviceType,
                this._timeZone
            );
         }
      }
      return new Builder();
   }
}

module.exports = VerifyPhoneRequest;

