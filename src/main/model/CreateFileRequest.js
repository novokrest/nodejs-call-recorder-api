class CreateFileRequest {
    constructor(
        apiKey,
        file,
        data
    ) {
        this.apiKey = apiKey;
        this.file = file;
        this.data = data;
    }

   static builder() {
      class Builder {
         apiKey(apiKey) {
            this._apiKey = apiKey;
            return this;
         }
         file(file) {
            this._file = file;
            return this;
         }
         data(data) {
            this._data = data;
            return this;
         }
         build() {
            return new CreateFileRequest(
                this._apiKey,
                this._file,
                this._data
            );
         }
      }
      return new Builder();
   }
}

module.exports = CreateFileRequest;

