const req = require('request');
const fs = require('fs');
const Configuration = require('./Configuration');
const BuyCreditsRequest = require('./model/BuyCreditsRequest');
const BuyCreditsResponse = require('./model/BuyCreditsResponse');
const CloneFileRequest = require('./model/CloneFileRequest');
const CloneFileResponse = require('./model/CloneFileResponse');
const CreateFileRequest = require('./model/CreateFileRequest');
const CreateFileResponse = require('./model/CreateFileResponse');
const CreateFolderRequest = require('./model/CreateFolderRequest');
const CreateFolderResponse = require('./model/CreateFolderResponse');
const DeleteFilesRequest = require('./model/DeleteFilesRequest');
const DeleteFilesResponse = require('./model/DeleteFilesResponse');
const DeleteFolderRequest = require('./model/DeleteFolderRequest');
const DeleteFolderResponse = require('./model/DeleteFolderResponse');
const DeleteMetaFilesRequest = require('./model/DeleteMetaFilesRequest');
const DeleteMetaFilesResponse = require('./model/DeleteMetaFilesResponse');
const GetFilesRequest = require('./model/GetFilesRequest');
const GetFilesResponse = require('./model/GetFilesResponse');
const GetFoldersRequest = require('./model/GetFoldersRequest');
const GetFoldersResponse = require('./model/GetFoldersResponse');
const GetLanguagesRequest = require('./model/GetLanguagesRequest');
const GetLanguagesResponse = require('./model/GetLanguagesResponse');
const GetMetaFilesRequest = require('./model/GetMetaFilesRequest');
const GetMetaFilesResponse = require('./model/GetMetaFilesResponse');
const GetMessagesRequest = require('./model/GetMessagesRequest');
const GetMessagesResponse = require('./model/GetMessagesResponse');
const GetPhonesRequest = require('./model/GetPhonesRequest');
const GetPhonesResponse = require('./model/GetPhonesResponse');
const GetProfileRequest = require('./model/GetProfileRequest');
const GetProfileResponse = require('./model/GetProfileResponse');
const GetSettingsRequest = require('./model/GetSettingsRequest');
const GetSettingsResponse = require('./model/GetSettingsResponse');
const GetTranslationsRequest = require('./model/GetTranslationsRequest');
const GetTranslationsResponse = require('./model/GetTranslationsResponse');
const NotifyUserRequest = require('./model/NotifyUserRequest');
const NotifyUserResponse = require('./model/NotifyUserResponse');
const RecoverFileRequest = require('./model/RecoverFileRequest');
const RecoverFileResponse = require('./model/RecoverFileResponse');
const RegisterPhoneRequest = require('./model/RegisterPhoneRequest');
const RegisterPhoneResponse = require('./model/RegisterPhoneResponse');
const UpdateDeviceTokenRequest = require('./model/UpdateDeviceTokenRequest');
const UpdateDeviceTokenResponse = require('./model/UpdateDeviceTokenResponse');
const UpdateFileRequest = require('./model/UpdateFileRequest');
const UpdateFileResponse = require('./model/UpdateFileResponse');
const UpdateFolderRequest = require('./model/UpdateFolderRequest');
const UpdateFolderResponse = require('./model/UpdateFolderResponse');
const UpdateOrderRequest = require('./model/UpdateOrderRequest');
const UpdateOrderResponse = require('./model/UpdateOrderResponse');
const UpdateProfileImgRequest = require('./model/UpdateProfileImgRequest');
const UpdateProfileImgResponse = require('./model/UpdateProfileImgResponse');
const UpdateProfileRequest = require('./model/UpdateProfileRequest');
const UpdateProfileResponse = require('./model/UpdateProfileResponse');
const UpdateSettingsRequest = require('./model/UpdateSettingsRequest');
const UpdateSettingsResponse = require('./model/UpdateSettingsResponse');
const UpdateStarRequest = require('./model/UpdateStarRequest');
const UpdateStarResponse = require('./model/UpdateStarResponse');
const UpdateUserRequest = require('./model/UpdateUserRequest');
const UpdateUserResponse = require('./model/UpdateUserResponse');
const UploadMetaFileRequest = require('./model/UploadMetaFileRequest');
const UploadMetaFileResponse = require('./model/UploadMetaFileResponse');
const VerifyFolderPassRequest = require('./model/VerifyFolderPassRequest');
const VerifyFolderPassResponse = require('./model/VerifyFolderPassResponse');
const VerifyPhoneRequest = require('./model/VerifyPhoneRequest');
const VerifyPhoneResponse = require('./model/VerifyPhoneResponse');

class CallRecorderApi {

    constructor(config) {
        this._config = config;
    }

    /**
      * @param request #BuyCreditsRequest
      * @param callback (#BuyCreditsResponse) => {}
      */
    buyCredits(request, callback) {
        const uri = this._config.baseUrl + '/rapi/buy_credits';
        const formParams = {
            "amount": request.amount,
            "api_key": request.apiKey,
            "device_type": request.deviceType,
            "product_id": request.productId,
            "receipt": request.receipt,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/buy_credits': " + err);
            }
            var json = JSON.parse(body);
            var response = BuyCreditsResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #CloneFileRequest
      * @param callback (#CloneFileResponse) => {}
      */
    cloneFile(request, callback) {
        const uri = this._config.baseUrl + '/rapi/clone_file';
        const formParams = {
            "api_key": request.apiKey,
            "id": request.id,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/clone_file': " + err);
            }
            var json = JSON.parse(body);
            var response = CloneFileResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .id(json.id)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #CreateFileRequest
      * @param callback (#CreateFileResponse) => {}
      */
    createFile(request, callback) {
        const uri = this._config.baseUrl + '/rapi/create_file';
        const formParams = {
            "api_key": request.apiKey,
            "data": JSON.stringify(request.data),
            "file": fs.createReadStream(request.file),
        };
        const options = {
            "method": "POST",
            "formData": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/create_file': " + err);
            }
            var json = JSON.parse(body);
            var response = CreateFileResponse.builder()
                .status(json.status)
                .id(json.id)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #CreateFolderRequest
      * @param callback (#CreateFolderResponse) => {}
      */
    createFolder(request, callback) {
        const uri = this._config.baseUrl + '/rapi/create_folder';
        const formParams = {
            "api_key": request.apiKey,
            "name": request.name,
            "pass": request.pass,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/create_folder': " + err);
            }
            var json = JSON.parse(body);
            var response = CreateFolderResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .id(json.id)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #DeleteFilesRequest
      * @param callback (#DeleteFilesResponse) => {}
      */
    deleteFiles(request, callback) {
        const uri = this._config.baseUrl + '/rapi/delete_files';
        const formParams = {
            "action": request.action,
            "api_key": request.apiKey,
            "ids": request.ids.join(','),
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/delete_files': " + err);
            }
            var json = JSON.parse(body);
            var response = DeleteFilesResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #DeleteFolderRequest
      * @param callback (#DeleteFolderResponse) => {}
      */
    deleteFolder(request, callback) {
        const uri = this._config.baseUrl + '/rapi/delete_folder';
        const formParams = {
            "api_key": request.apiKey,
            "id": request.id,
            "move_to": request.moveTo,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/delete_folder': " + err);
            }
            var json = JSON.parse(body);
            var response = DeleteFolderResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #DeleteMetaFilesRequest
      * @param callback (#DeleteMetaFilesResponse) => {}
      */
    deleteMetaFiles(request, callback) {
        const uri = this._config.baseUrl + '/rapi/delete_meta_files';
        const formParams = {
            "api_key": request.apiKey,
            "ids": request.ids.join(','),
            "parent_id": request.parentId,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/delete_meta_files': " + err);
            }
            var json = JSON.parse(body);
            var response = DeleteMetaFilesResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetFilesRequest
      * @param callback (#GetFilesResponse) => {}
      */
    getFiles(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_files';
        const formParams = {
            "api_key": request.apiKey,
            "folder_id": request.folderId,
            "id": request.id,
            "op": request.op,
            "page": request.page,
            "pass": request.pass,
            "q": request.q,
            "reminder": request.reminder,
            "source": request.source,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_files': " + err);
            }
            var json = JSON.parse(body);
            var response = GetFilesResponse.builder()
                .status(json.status)
                .credits(json.credits)
                .creditsTrans(json.credits_trans)
                .files(json.files)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetFoldersRequest
      * @param callback (#GetFoldersResponse) => {}
      */
    getFolders(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_folders';
        const formParams = {
            "api_key": request.apiKey,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_folders': " + err);
            }
            var json = JSON.parse(body);
            var response = GetFoldersResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .folders(json.folders)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetLanguagesRequest
      * @param callback (#GetLanguagesResponse) => {}
      */
    getLanguages(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_languages';
        const formParams = {
            "api_key": request.apiKey,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_languages': " + err);
            }
            var json = JSON.parse(body);
            var response = GetLanguagesResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .languages(json.languages)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetMetaFilesRequest
      * @param callback (#GetMetaFilesResponse) => {}
      */
    getMetaFiles(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_meta_files';
        const formParams = {
            "api_key": request.apiKey,
            "parent_id": request.parentId,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_meta_files': " + err);
            }
            var json = JSON.parse(body);
            var response = GetMetaFilesResponse.builder()
                .status(json.status)
                .metaFiles(json.meta_files)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetMessagesRequest
      * @param callback (#GetMessagesResponse) => {}
      */
    getMsgs(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_msgs';
        const formParams = {
            "api_key": request.apiKey,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_msgs': " + err);
            }
            var json = JSON.parse(body);
            var response = GetMessagesResponse.builder()
                .status(json.status)
                .msgs(json.msgs)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetPhonesRequest
      * @param callback (#GetPhonesResponse) => {}
      */
    getPhones(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_phones';
        const formParams = {
            "api_key": request.apiKey,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_phones': " + err);
            }
            var json = JSON.parse(body);
            var response = GetPhonesResponse.builder()
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetProfileRequest
      * @param callback (#GetProfileResponse) => {}
      */
    getProfile(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_profile';
        const formParams = {
            "api_key": request.apiKey,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_profile': " + err);
            }
            var json = JSON.parse(body);
            var response = GetProfileResponse.builder()
                .status(json.status)
                .code(json.code)
                .profile(json.profile)
                .app(json.app)
                .shareUrl(json.share_url)
                .rateUrl(json.rate_url)
                .credits(json.credits)
                .creditsTrans(json.credits_trans)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetSettingsRequest
      * @param callback (#GetSettingsResponse) => {}
      */
    getSettings(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_settings';
        const formParams = {
            "api_key": request.apiKey,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_settings': " + err);
            }
            var json = JSON.parse(body);
            var response = GetSettingsResponse.builder()
                .status(json.status)
                .app(json.app)
                .credits(json.credits)
                .settings(json.settings)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #GetTranslationsRequest
      * @param callback (#GetTranslationsResponse) => {}
      */
    getTranslations(request, callback) {
        const uri = this._config.baseUrl + '/rapi/get_translations';
        const formParams = {
            "api_key": request.apiKey,
            "language": request.language,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/get_translations': " + err);
            }
            var json = JSON.parse(body);
            var response = GetTranslationsResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .translation(json.translation)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #NotifyUserRequest
      * @param callback (#NotifyUserResponse) => {}
      */
    notifyUserCustom(request, callback) {
        const uri = this._config.baseUrl + '/rapi/notify_user_custom';
        const formParams = {
            "api_key": request.apiKey,
            "body": request.body,
            "device_type": request.deviceType,
            "title": request.title,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/notify_user_custom': " + err);
            }
            var json = JSON.parse(body);
            var response = NotifyUserResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #RecoverFileRequest
      * @param callback (#RecoverFileResponse) => {}
      */
    recoverFile(request, callback) {
        const uri = this._config.baseUrl + '/rapi/recover_file';
        const formParams = {
            "api_key": request.apiKey,
            "folder_id": request.folderId,
            "id": request.id,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/recover_file': " + err);
            }
            var json = JSON.parse(body);
            var response = RecoverFileResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #RegisterPhoneRequest
      * @param callback (#RegisterPhoneResponse) => {}
      */
    registerPhone(request, callback) {
        const uri = this._config.baseUrl + '/rapi/register_phone';
        const formParams = {
            "phone": request.phone,
            "token": request.token,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/register_phone': " + err);
            }
            var json = JSON.parse(body);
            var response = RegisterPhoneResponse.builder()
                .status(json.status)
                .phone(json.phone)
                .code(json.code)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateDeviceTokenRequest
      * @param callback (#UpdateDeviceTokenResponse) => {}
      */
    updateDeviceToken(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_device_token';
        const formParams = {
            "api_key": request.apiKey,
            "device_token": request.deviceToken,
            "device_type": request.deviceType,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_device_token': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateDeviceTokenResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateFileRequest
      * @param callback (#UpdateFileResponse) => {}
      */
    updateFile(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_file';
        const formParams = {
            "api_key": request.apiKey,
            "email": request.email,
            "f_name": request.fName,
            "folder_id": request.folderId,
            "id": request.id,
            "l_name": request.lName,
            "name": request.name,
            "notes": request.notes,
            "phone": request.phone,
            "remind_date": request.remindDate,
            "remind_days": request.remindDays,
            "tags": request.tags,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_file': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateFileResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateFolderRequest
      * @param callback (#UpdateFolderResponse) => {}
      */
    updateFolder(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_folder';
        const formParams = {
            "api_key": request.apiKey,
            "id": request.id,
            "is_private": request.isPrivate,
            "name": request.name,
            "pass": request.pass,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_folder': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateFolderResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateOrderRequest
      * @param callback (#UpdateOrderResponse) => {}
      */
    updateOrder(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_order';
        const formParams = {
            "api_key": request.apiKey,
            "folders": request.folders.join(','),
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_order': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateOrderResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateProfileImgRequest
      * @param callback (#UpdateProfileImgResponse) => {}
      */
    updateProfileImg(request, callback) {
        const uri = this._config.baseUrl + '/upload/update_profile_img';
        const formParams = {
            "api_key": request.apiKey,
            "file": fs.createReadStream(request.file),
        };
        const options = {
            "method": "POST",
            "formData": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_profile_img': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateProfileImgResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .file(json.file)
                .path(json.path)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateProfileRequest
      * @param callback (#UpdateProfileResponse) => {}
      */
    updateProfile(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_profile';
        const formParams = {
            "api_key": request.apiKey,
            "data[f_name]": request.data.fName,
            "data[l_name]": request.data.lName,
            "data[email]": request.data.email,
            "data[is_public]": request.data.isPublic,
            "data[language]": request.data.language,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_profile': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateProfileResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateSettingsRequest
      * @param callback (#UpdateSettingsResponse) => {}
      */
    updateSettings(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_settings';
        const formParams = {
            "api_key": request.apiKey,
            "files_permission": request.filesPermission,
            "play_beep": request.playBeep,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_settings': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateSettingsResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateStarRequest
      * @param callback (#UpdateStarResponse) => {}
      */
    updateStar(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_star';
        const formParams = {
            "api_key": request.apiKey,
            "id": request.id,
            "star": request.star,
            "type": request.type,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_star': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateStarResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UpdateUserRequest
      * @param callback (#UpdateUserResponse) => {}
      */
    updateUser(request, callback) {
        const uri = this._config.baseUrl + '/rapi/update_user';
        const formParams = {
            "api_key": request.apiKey,
            "app": request.app,
            "time_zone": request.timeZone,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/update_user': " + err);
            }
            var json = JSON.parse(body);
            var response = UpdateUserResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #UploadMetaFileRequest
      * @param callback (#UploadMetaFileResponse) => {}
      */
    uploadMetaFile(request, callback) {
        const uri = this._config.baseUrl + '/rapi/upload_meta_file';
        const formParams = {
            "api_key": request.apiKey,
            "file": fs.createReadStream(request.file),
            "id": request.id,
            "name": request.name,
            "parent_id": request.parentId,
        };
        const options = {
            "method": "POST",
            "formData": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/upload_meta_file': " + err);
            }
            var json = JSON.parse(body);
            var response = UploadMetaFileResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .parentId(json.parent_id)
                .id(json.id)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #VerifyFolderPassRequest
      * @param callback (#VerifyFolderPassResponse) => {}
      */
    verifyFolderPass(request, callback) {
        const uri = this._config.baseUrl + '/rapi/verify_folder_pass';
        const formParams = {
            "api_key": request.apiKey,
            "id": request.id,
            "pass": request.pass,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/verify_folder_pass': " + err);
            }
            var json = JSON.parse(body);
            var response = VerifyFolderPassResponse.builder()
                .status(json.status)
                .msg(json.msg)
                .code(json.code)
                .build();
            callback(response);
        });
    }

    /**
      * @param request #VerifyPhoneRequest
      * @param callback (#VerifyPhoneResponse) => {}
      */
    verifyPhone(request, callback) {
        const uri = this._config.baseUrl + '/rapi/verify_phone';
        const formParams = {
            "app": request.app,
            "code": request.code,
            "device_id": request.deviceId,
            "device_token": request.deviceToken,
            "device_type": request.deviceType,
            "mcc": request.mcc,
            "phone": request.phone,
            "time_zone": request.timeZone,
            "token": request.token,
        };
        const options = {
            "method": "POST",
            "form": formParams,
            "strictSSL": false,
        };
        req(uri, options, (err, response, body) => {
            if (err) {
                throw Error("Error occurred when calls to '/verify_phone': " + err);
            }
            var json = JSON.parse(body);
            var response = VerifyPhoneResponse.builder()
                .status(json.status)
                .phone(json.phone)
                .apiKey(json.api_key)
                .msg(json.msg)
                .build();
            callback(response);
        });
    }

}

module.exports = CallRecorderApi;
