class Configuration {

    constructor(baseUrl) {
        this._baseUrl = baseUrl;
    }

    get baseUrl() {
        return this._baseUrl;
    }

    static builder() {
        class Builder {

            baseUrl(baseUrl) {
                this._baseUrl = baseUrl;
                return this;
            }

            build() {
                return new Configuration(
                    this._baseUrl,
                );
            }
        }
        return new Builder();
     }

}

module.exports = Configuration;
