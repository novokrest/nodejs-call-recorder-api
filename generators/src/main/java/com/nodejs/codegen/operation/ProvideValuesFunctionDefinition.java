package com.nodejs.codegen.operation;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class ProvideValuesFunctionDefinition {

    private final String functionName;

    private final String functionBody;

    private ProvideValuesFunctionDefinition(
        @Nonnull String functionName,
        @Nonnull String functionBody
    ) {
        this.functionName = requireNonNull(functionName, "functionName");
        this.functionBody = requireNonNull(functionBody, "functionBody");
    }

    @Nonnull
    public String getFunctionName() {
        return functionName;
    }

    @Nonnull
    public String getFunctionBody() {
        return functionBody;
    }

    @Nonnull
    @Override
    public String toString() {
        return "ProvideValuesFunctionDefinition{" +
            "functionName='" + functionName + '\'' +
            ", functionBody='" + functionBody + '\'' +
            '}';
    }


    public static class Builder {

        private String functionName;
        private String functionBody;

        @Nonnull
        public static Builder provideValuesFunctionDefinition() {
            return new Builder();
        }

        private Builder() {
        }

        @Nonnull
        public Builder withProvidedValueNames(@Nonnull String functionName) {
            this.functionName = functionName;
            return this;
        }

        @Nonnull
        public Builder withFunctionBody(@Nonnull String functionBody) {
            this.functionBody = functionBody;
            return this;
        }

        @Nonnull
        public ProvideValuesFunctionDefinition build() {
            return new ProvideValuesFunctionDefinition(
                functionName,
                functionBody
            );
        }

    }

}

