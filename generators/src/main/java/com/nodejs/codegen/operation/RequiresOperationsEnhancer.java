package com.nodejs.codegen.operation;

import io.swagger.codegen.v3.CodegenModel;

import java.util.List;
import java.util.stream.Collectors;

/**
 * TODO:
 */
public class RequiresOperationsEnhancer implements OperationsEnhancer {

    @Override
    public void enhance(OperationsContext operationsContext) {
        List<RequireModel> requires = operationsContext.getModels().values().stream()
            .map(CodegenModel::getClassname)
            .sorted()
            .map(RequireModel::new)
            .collect(Collectors.toList());
        operationsContext.setRequires(requires);
    }

    public static final class RequireModel {

        private final String modelName;

        private RequireModel(String modelName) {
            this.modelName = modelName;
        }

        public String getModelName() {
            return modelName;
        }

    }

}
