package com.nodejs.codegen.operation;

import io.swagger.codegen.v3.CodegenModel;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class ProvideApiKeyDefinition {

    private final String name;

    private final String registerPhoneOperationName;

    private final CodegenModel registerPhoneRequestModel;

    private final String verifyPhoneOperationName;

    private final CodegenModel verifyPhoneRequestModel;

    private final String verifyPhoneResponseApiKeyPropertyName;

    private ProvideApiKeyDefinition(
        @Nonnull String name,
        @Nonnull String registerPhoneOperationName,
        @Nonnull CodegenModel registerPhoneRequestModel,
        @Nonnull String verifyPhoneOperationName,
        @Nonnull CodegenModel verifyPhoneRequestModel,
        @Nonnull String verifyPhoneResponseApiKeyPropertyName
    ) {
        this.name = requireNonNull(name, "name");
        this.registerPhoneOperationName = requireNonNull(registerPhoneOperationName, "registerPhoneOperationName");
        this.registerPhoneRequestModel = requireNonNull(registerPhoneRequestModel, "registerPhoneRequestModel");
        this.verifyPhoneOperationName = requireNonNull(verifyPhoneOperationName, "verifyPhoneOperationName");
        this.verifyPhoneRequestModel = requireNonNull(verifyPhoneRequestModel, "verifyPhoneRequestModel");
        this.verifyPhoneResponseApiKeyPropertyName = requireNonNull(verifyPhoneResponseApiKeyPropertyName, "verifyPhoneResponseApiKeyPropertyName");
    }

    @Nonnull
    public String getName() {
        return name;
    }

    @Nonnull
    public String getRegisterPhoneOperationName() {
        return registerPhoneOperationName;
    }

    @Nonnull
    public CodegenModel getRegisterPhoneRequestModel() {
        return registerPhoneRequestModel;
    }

    @Nonnull
    public String getVerifyPhoneOperationName() {
        return verifyPhoneOperationName;
    }

    @Nonnull
    public CodegenModel getVerifyPhoneRequestModel() {
        return verifyPhoneRequestModel;
    }

    @Nonnull
    public String getVerifyPhoneResponseApiKeyPropertyName() {
        return verifyPhoneResponseApiKeyPropertyName;
    }

    @Nonnull
    @Override
    public String toString() {
        return "ProvideApiKeyDefinition{" +
            "name='" + name + '\'' +
            ", registerPhoneOperationName='" + registerPhoneOperationName + '\'' +
            ", registerPhoneRequestModel=" + registerPhoneRequestModel +
            ", verifyPhoneOperationName='" + verifyPhoneOperationName + '\'' +
            ", verifyPhoneRequestModel=" + verifyPhoneRequestModel +
            ", verifyPhoneResponseApiKeyPropertyName='" + verifyPhoneResponseApiKeyPropertyName + '\'' +
            '}';
    }

    public static class Builder {

        private String name;
        private String registerPhoneOperationName;
        private CodegenModel registerPhoneRequestModel;
        private String verifyPhoneOperationName;
        private CodegenModel verifyPhoneRequestModel;
        private String verifyPhoneResponseApiKeyPropertyName;

        private Builder() {
        }

        public static Builder provideApiKeyDefinition() {
            return new Builder();
        }

        @Nonnull
        public Builder withName(@Nonnull String name) {
            this.name = name;
            return this;
        }

        @Nonnull
        public Builder withRegisterPhoneOperationName(@Nonnull String registerPhoneOperationName) {
            this.registerPhoneOperationName = registerPhoneOperationName;
            return this;
        }

        @Nonnull
        public Builder withRegisterPhoneRequestModel(@Nonnull CodegenModel registerPhoneRequestModel) {
            this.registerPhoneRequestModel = registerPhoneRequestModel;
            return this;
        }

        @Nonnull
        public Builder withVerifyPhoneOperationName(@Nonnull String verifyPhoneOperationName) {
            this.verifyPhoneOperationName = verifyPhoneOperationName;
            return this;
        }

        @Nonnull
        public Builder withVerifyPhoneRequestModel(@Nonnull CodegenModel verifyPhoneRequestModel) {
            this.verifyPhoneRequestModel = verifyPhoneRequestModel;
            return this;
        }

        @Nonnull
        public Builder withVerifyPhoneResponseApiKeyPropertyName(@Nonnull String verifyPhoneResponseApiKeyPropertyName) {
            this.verifyPhoneResponseApiKeyPropertyName = verifyPhoneResponseApiKeyPropertyName;
            return this;
        }

        @Nonnull
        public ProvideApiKeyDefinition build() {
            return new ProvideApiKeyDefinition(
                name,
                registerPhoneOperationName,
                registerPhoneRequestModel,
                verifyPhoneOperationName,
                verifyPhoneRequestModel,
                verifyPhoneResponseApiKeyPropertyName
            );
        }

    }

}
