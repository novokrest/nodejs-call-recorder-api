package com.nodejs.codegen.operation;

import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

import java.util.List;

public class ComplexPropertyEnhancer implements OperationsEnhancer {

    @Override
    public void enhance(OperationsContext operationsContext) {
        operationsContext.getModels().values().stream()
            .map(CodegenModel::getVars)
            .flatMap(List::stream)
            .forEach(property -> enhanceProperty(operationsContext, property));
    }

    private void enhanceProperty(OperationsContext operationsContext, CodegenProperty property) {
        CodegenPropertyEx.setComplex(property, isComplex(operationsContext, property));
    }

    private boolean isComplex(OperationsContext operationsContext, CodegenProperty property) {
        String dataType = property.getDatatype();
        return hasModelType(operationsContext, dataType) && isNotEnum(operationsContext, dataType);
    }

    private boolean hasModelType(OperationsContext operationsContext, String dataType) {
        return operationsContext.getModels().values().stream().anyMatch(model -> model.classname.equals(dataType));
    }

    private boolean isNotEnum(OperationsContext operationsContext, String dataType) {
        CodegenModel propertyModel = operationsContext.getModels().values().stream().filter(model -> model.classname.equals(dataType)).findFirst().get();
        return !propertyModel.getIsEnum();
    }
}
