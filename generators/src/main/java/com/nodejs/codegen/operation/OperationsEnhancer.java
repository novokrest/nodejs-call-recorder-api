package com.nodejs.codegen.operation;

public interface OperationsEnhancer {

    void enhance(OperationsContext operationsContext);

}
