package com.nodejs.codegen.operation;

import com.nodejs.codegen.extensions.CodegenOperationEx;
import io.swagger.codegen.v3.CodegenOperation;

public class FormParameterNameOperationsEnhancer implements OperationsEnhancer {

    @Override
    public void enhance(OperationsContext operationsContext) {
        operationsContext.getOperations().forEach(this::enhanceOperation);
    }

    private void enhanceOperation(CodegenOperation operation) {
        if (hasMultipartFormData(operation)) {
            CodegenOperationEx.setFormParameterName(operation, "formData");
        } else {
            CodegenOperationEx.setFormParameterName(operation, "form");
        }
    }

    private boolean hasMultipartFormData(CodegenOperation operation) {
        return operation.contents.stream().anyMatch(content -> content.getContentType().equals("multipart/form-data"));
    }

}
