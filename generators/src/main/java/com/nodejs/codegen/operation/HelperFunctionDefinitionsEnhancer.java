package com.nodejs.codegen.operation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import com.nodejs.codegen.core.WellKnown;
import com.nodejs.codegen.value.GlobalProvidedValues;

import java.util.List;
import java.util.stream.Collectors;

import static com.nodejs.codegen.operation.ProvideValuesBySequentialOperationCallsFunctionDescription.Builder.provideValuesBySequentialOperationCallsFunctionDescription;
import static com.nodejs.codegen.operation.ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall.Builder.operationCall;
import static com.nodejs.codegen.operation.ProvideValuesBySequentialOperationCallsFunctionDescription.OperationResponseProperty.Builder.operationResponseProperty;
import static com.nodejs.codegen.operation.ProvideValuesBySequentialOperationCallsFunctionDescription.ProvidedValue.Builder.providedValue;
import static java.util.Collections.emptyList;
import static java.util.Collections.emptyMap;

public class HelperFunctionDefinitionsEnhancer implements OperationsEnhancer {

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall REGISTER_PHONE_FOR_CODE_OPERATION_CALL =
        operationCall()
            .withOperationId(WellKnown.REGISTER_PHONE_OPERATION_ID)
            .withInputRequestParameters(emptyMap())
            .withCachedResponsePropertyNames(WellKnown.REGISTER_PHONE_RESPONSE_CODE_PROPERTY_NAME)
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall VERIFY_PHONE_CODE_FOR_API_KEY_OPERATION_CALL =
        operationCall()
            .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
            .withInputRequestParameters(
                WellKnown.VERIFY_PHONE_REQUEST_CODE_PROPERTY_NAME,
                operationResponseProperty()
                    .withOperationId(WellKnown.REGISTER_PHONE_OPERATION_ID)
                    .withResponsePropertyName(WellKnown.REGISTER_PHONE_RESPONSE_CODE_PROPERTY_NAME)
                    .build()
            )
            .withCachedResponsePropertyNames(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall CREATE_FILE_FOR_FILE_ID_OPERATION_CALL =
        operationCall()
            .withOperationId(WellKnown.CREATE_FILE_OPERATION_ID)
            .withInputRequestParameters(WellKnown.COMMON_REQUEST_API_KEY_PROPERTY_NAME, operationResponseProperty()
                .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
                .withResponsePropertyName(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
                .build())
            .withCachedResponsePropertyNames(WellKnown.CREATE_FILE_RESPONSE_FILE_ID_PROPERTY_NAME)
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall CREATE_META_FILE_FOR_META_FILE_ID_OPERATION_CALL =
        operationCall()
            .withOperationId(WellKnown.UPLOAD_META_FILE_OPERATION_ID)
            .withInputRequestParameters(ImmutableMap.<String, ProvideValuesBySequentialOperationCallsFunctionDescription.OperationResponseProperty>builder()
                .put(WellKnown.COMMON_REQUEST_API_KEY_PROPERTY_NAME, operationResponseProperty()
                    .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
                    .withResponsePropertyName(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
                    .build())
                .put(WellKnown.UPLOAD_META_FILE_REQUEST_PARENT_ID_PROPERTY_NAME, operationResponseProperty()
                    .withOperationId(WellKnown.CREATE_FILE_OPERATION_ID)
                    .withResponsePropertyName(WellKnown.CREATE_FILE_RESPONSE_FILE_ID_PROPERTY_NAME)
                    .build())
                .build())
            .withCachedResponsePropertyNames(WellKnown.UPLOAD_META_FILE_RESPONSE_ID_PROPERTY_NAME)
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall CREATE_FOLDER_FOR_FOLDER_ID_OPERATION_CALL =
        operationCall()
            .withOperationId(WellKnown.CREATE_FOLDER_OPERATION_ID)
            .withInputRequestParameters(WellKnown.COMMON_REQUEST_API_KEY_PROPERTY_NAME, operationResponseProperty()
                .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
                .withResponsePropertyName(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
                .build())
            .withCachedResponsePropertyNames(WellKnown.CREATE_FOLDER_RESPONSE_FOLDER_ID_PROPERTY_NAME)
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription PROVIDE_VERIFICATION_CODE_DESCRIPTION =
        provideValuesBySequentialOperationCallsFunctionDescription()
            .withInputParameters(emptyList())
            .withOperationCalls(ImmutableList.of(
                REGISTER_PHONE_FOR_CODE_OPERATION_CALL
            ))
            .withProvidedValues(providedValue()
                .withOperationResponseProperty(operationResponseProperty()
                    .withOperationId(WellKnown.REGISTER_PHONE_OPERATION_ID)
                    .withResponsePropertyName(WellKnown.REGISTER_PHONE_RESPONSE_CODE_PROPERTY_NAME)
                    .build())
                .withValueName(GlobalProvidedValues.CODE.getValueName())
                .build())
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription PROVIDE_API_KEY_DESCRIPTION =
        provideValuesBySequentialOperationCallsFunctionDescription()
            .withInputParameters(emptyList())
            .withOperationCalls(ImmutableList.of(
                REGISTER_PHONE_FOR_CODE_OPERATION_CALL,
                VERIFY_PHONE_CODE_FOR_API_KEY_OPERATION_CALL
            ))
            .withProvidedValues(providedValue()
                .withOperationResponseProperty(operationResponseProperty()
                    .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
                    .withResponsePropertyName(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
                    .build())
                .withValueName(GlobalProvidedValues.API_KEY.getValueName())
                .build())
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription PROVIDE_API_KEY_AND_FILE_ID_DESCRIPTION =
        provideValuesBySequentialOperationCallsFunctionDescription()
            .withInputParameters(emptyList())
            .withOperationCalls(ImmutableList.of(
                REGISTER_PHONE_FOR_CODE_OPERATION_CALL,
                VERIFY_PHONE_CODE_FOR_API_KEY_OPERATION_CALL,
                CREATE_FILE_FOR_FILE_ID_OPERATION_CALL
            ))
            .withProvidedValues(
                providedValue()
                    .withOperationResponseProperty(operationResponseProperty()
                        .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
                        .withResponsePropertyName(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
                        .build())
                    .withValueName(GlobalProvidedValues.API_KEY.getValueName())
                    .build(),
                providedValue()
                    .withOperationResponseProperty(operationResponseProperty()
                        .withOperationId(WellKnown.CREATE_FILE_OPERATION_ID)
                        .withResponsePropertyName(WellKnown.CREATE_FILE_RESPONSE_FILE_ID_PROPERTY_NAME)
                        .build())
                    .withValueName(GlobalProvidedValues.FILE_ID.getValueName())
                    .build()
            )
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription PROVIDE_API_KEY_AND_FILE_ID_AND_META_FILE_ID_DESCRIPTION =
        provideValuesBySequentialOperationCallsFunctionDescription()
            .withInputParameters(emptyList())
            .withOperationCalls(ImmutableList.of(
                REGISTER_PHONE_FOR_CODE_OPERATION_CALL,
                VERIFY_PHONE_CODE_FOR_API_KEY_OPERATION_CALL,
                CREATE_FILE_FOR_FILE_ID_OPERATION_CALL,
                CREATE_META_FILE_FOR_META_FILE_ID_OPERATION_CALL
            ))
            .withProvidedValues(
                providedValue()
                    .withOperationResponseProperty(operationResponseProperty()
                        .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
                        .withResponsePropertyName(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
                        .build())
                    .withValueName(GlobalProvidedValues.API_KEY.getValueName())
                    .build(),
                providedValue()
                    .withOperationResponseProperty(operationResponseProperty()
                        .withOperationId(WellKnown.CREATE_FILE_OPERATION_ID)
                        .withResponsePropertyName(WellKnown.CREATE_FILE_RESPONSE_FILE_ID_PROPERTY_NAME)
                        .build())
                    .withValueName(GlobalProvidedValues.FILE_ID.getValueName())
                    .build(),
                providedValue()
                    .withOperationResponseProperty(operationResponseProperty()
                        .withOperationId(WellKnown.UPLOAD_META_FILE_OPERATION_ID)
                        .withResponsePropertyName(WellKnown.UPLOAD_META_FILE_RESPONSE_ID_PROPERTY_NAME)
                        .build())
                    .withValueName(GlobalProvidedValues.META_FILE_ID.getValueName())
                    .build()
            )
            .build();

    private static final ProvideValuesBySequentialOperationCallsFunctionDescription PROVIDE_API_KEY_AND_FOLDER_ID_DESCRIPTION =
        provideValuesBySequentialOperationCallsFunctionDescription()
            .withInputParameters(emptyList())
            .withOperationCalls(ImmutableList.of(
                REGISTER_PHONE_FOR_CODE_OPERATION_CALL,
                VERIFY_PHONE_CODE_FOR_API_KEY_OPERATION_CALL,
                CREATE_FOLDER_FOR_FOLDER_ID_OPERATION_CALL
            ))
            .withProvidedValues(
                providedValue()
                    .withOperationResponseProperty(operationResponseProperty()
                        .withOperationId(WellKnown.VERIFY_PHONE_OPERATION_ID)
                        .withResponsePropertyName(WellKnown.VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME)
                        .build())
                    .withValueName(GlobalProvidedValues.API_KEY.getValueName())
                    .build(),
                providedValue()
                    .withOperationResponseProperty(operationResponseProperty()
                        .withOperationId(WellKnown.CREATE_FOLDER_OPERATION_ID)
                        .withResponsePropertyName(WellKnown.CREATE_FOLDER_RESPONSE_FOLDER_ID_PROPERTY_NAME)
                        .build())
                    .withValueName(GlobalProvidedValues.FOLDER_ID.getValueName())
                    .build()
            )
            .build();

    @Override
    public void enhance(OperationsContext operationsContext) {
        operationsContext.setProperty(
            "provide-value-function-defs",
            ImmutableMap.<String, Object>builder()
                .put("provide-value-function-def", createProvideValueFunctionDefinitions(operationsContext))
                .build()
        );
    }

    private List<ProvideValuesFunctionDefinition> createProvideValueFunctionDefinitions(OperationsContext operationsContext) {
        ProvideValuesFunctionDefinitionFactory functionDefinitionFactory = new ProvideValuesFunctionDefinitionFactory(operationsContext);
        return ImmutableList.<ProvideValuesBySequentialOperationCallsFunctionDescription>builder()
            .add(PROVIDE_VERIFICATION_CODE_DESCRIPTION)
            .add(PROVIDE_API_KEY_DESCRIPTION)
            .add(PROVIDE_API_KEY_AND_FILE_ID_DESCRIPTION)
            .add(PROVIDE_API_KEY_AND_FOLDER_ID_DESCRIPTION)
            .add(PROVIDE_API_KEY_AND_FILE_ID_AND_META_FILE_ID_DESCRIPTION)
            .build()
            .stream()
            .map(functionDefinitionFactory::getDefinitionForSequentialOperationInvocations)
            .collect(Collectors.toList());
    }

}
