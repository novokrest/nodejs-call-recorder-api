package com.nodejs.codegen.operation;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenOperation;

import java.util.List;
import java.util.Map;

import static java.util.function.Function.identity;
import static java.util.stream.Collectors.toMap;

public class OperationsContext {

    private final java.util.Map<String, Object> objs;
    private final Map<String, CodegenModel> modelByName;

    public static OperationsContext fromOperationsAndModels(Map<String, Object> objs, List<Object> allModels) {
        Map<String, CodegenModel> modelByName = allModels.stream()
            .map(obj -> (Map<String, Object>) obj)
            .map(obj -> (CodegenModel) obj.get("model"))
            .collect(toMap(CodegenModel::getName, identity()));
        return new OperationsContext(objs, modelByName);
    }

    private OperationsContext(Map<String, Object> objs, Map<String, CodegenModel> modelByName) {
        this.objs = objs;
        this.modelByName = modelByName;
    }

    public List<CodegenOperation> getOperations() {
        Map<String, Object> operations = (Map<String, Object>) objs.get("operations");
        List<CodegenOperation> operationList = (List<CodegenOperation>) operations.get("operation");
        return ImmutableList.copyOf(operationList);
    }

    public Map<String, CodegenModel> getModels() {
        return ImmutableMap.copyOf(modelByName);
    }

    public void setProperty(String property, Object value) {
        objs.put(property, value);
    }

    public CodegenOperation getOperationById(String operationId) {
        return getOperations().stream()
            .filter(operation -> operation.operationId.equals(operationId))
            .findFirst()
            .orElseThrow(() -> new IllegalArgumentException("Operation was not found: operationId=" + operationId));
    }

    public void setRequires(List<RequiresOperationsEnhancer.RequireModel> requires) {
        objs.put("requires", requires);
    }
}
