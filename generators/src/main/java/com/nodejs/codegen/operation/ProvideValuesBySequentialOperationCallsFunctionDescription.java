package com.nodejs.codegen.operation;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;

public class ProvideValuesBySequentialOperationCallsFunctionDescription {

    private final List<String> inputParameters;
    private final List<OperationCall> operationCalls;
    private final List<ProvidedValue> providedValues;

    public ProvideValuesBySequentialOperationCallsFunctionDescription(List<String> inputParameters,
                                                                      List<OperationCall> operationCalls,
                                                                      List<ProvidedValue> providedValues) {
        this.inputParameters = inputParameters;
        this.operationCalls = operationCalls;
        this.providedValues = providedValues;
    }

    public List<String> getInputParameters() {
        return inputParameters;
    }

    public List<OperationCall> getOperationCalls() {
        return operationCalls;
    }

    public List<ProvidedValue> getProvidedValues() {
        return providedValues;
    }

    public static class Builder {

        private List<String> inputParameters;
        private List<OperationCall> operationCalls;
        private List<ProvidedValue> providedValues;

        public static Builder provideValuesBySequentialOperationCallsFunctionDescription() {
            return new Builder();
        }

        private Builder() {
        }

        @Nonnull
        public Builder withInputParameters(@Nonnull List<String> inputParameters) {
            this.inputParameters = inputParameters;
            return this;
        }

        @Nonnull
        public Builder withOperationCalls(@Nonnull List<OperationCall> operationCalls) {
            this.operationCalls = operationCalls;
            return this;
        }

        @Nonnull
        public Builder withProvidedValues(@Nonnull ProvidedValue providedValue) {
            this.providedValues = singletonList(providedValue);
            return this;
        }

        @Nonnull
        public Builder withProvidedValues(@Nonnull ProvidedValue... providedValues) {
            this.providedValues = Arrays.stream(providedValues).collect(Collectors.toList());
            return this;
        }

        @Nonnull
        public Builder withProvidedValues(@Nonnull List<ProvidedValue> providedValues) {
            this.providedValues = providedValues;
            return this;
        }

        @Nonnull
        public ProvideValuesBySequentialOperationCallsFunctionDescription build() {
            return new ProvideValuesBySequentialOperationCallsFunctionDescription(
                inputParameters,
                operationCalls,
                providedValues
            );
        }

    }

    public static class OperationCall {

        private final String operationId;
        private final Map<String, OperationResponseProperty> inputRequestParameters;
        private final List<String> cachedResponsePropertyNames;

        public OperationCall(String operationId,
                             Map<String, OperationResponseProperty> inputRequestParameters,
                             List<String> cachedResponsePropertyNames) {
            this.operationId = operationId;
            this.inputRequestParameters = inputRequestParameters;
            this.cachedResponsePropertyNames = cachedResponsePropertyNames;
        }

        public String getOperationId() {
            return operationId;
        }

        public Set<String> getInputRequestParameterNames() {
            return inputRequestParameters.keySet();
        }

        public Map<String, OperationResponseProperty> getInputRequestParameters() {
            return inputRequestParameters;
        }

        public List<String> getCachedResponsePropertyNames() {
            return cachedResponsePropertyNames;
        }

        public static class Builder {

            private String operationId;
            private Map<String, OperationResponseProperty> inputRequestParameters;
            private List<String> cachedResponsePropertyNames;

            public static Builder operationCall() {
                return new Builder();
            }

            private Builder() {
            }

            @Nonnull
            public Builder withOperationId(@Nonnull String operationId) {
                this.operationId = operationId;
                return this;
            }

            @Nonnull
            public Builder withInputRequestParameters(@Nonnull String propertyName, OperationResponseProperty operationResponseProperty) {
                this.inputRequestParameters = singletonMap(propertyName, operationResponseProperty);
                return this;
            }

            @Nonnull
            public Builder withInputRequestParameters(@Nonnull Map<String, OperationResponseProperty> inputRequestParameters) {
                this.inputRequestParameters = inputRequestParameters;
                return this;
            }

            @Nonnull
            public Builder withCachedResponsePropertyNames(@Nonnull String cachedResponsePropertyName) {
                this.cachedResponsePropertyNames = singletonList(cachedResponsePropertyName);
                return this;
            }

            @Nonnull
            public Builder withCachedResponsePropertyNames(@Nonnull List<String> cachedResponsePropertyNames) {
                this.cachedResponsePropertyNames = cachedResponsePropertyNames;
                return this;
            }

            @Nonnull
            public OperationCall build() {
                return new OperationCall(
                    operationId,
                    inputRequestParameters,
                    cachedResponsePropertyNames
                );
            }

        }

    }

    public static class ProvidedValue {

        private final String valueName;
        private final OperationResponseProperty operationResponseProperty;

        public ProvidedValue(String valueName, OperationResponseProperty operationResponseProperty) {
            this.valueName = valueName;
            this.operationResponseProperty = operationResponseProperty;
        }

        public String getValueName() {
            return valueName;
        }

        public OperationResponseProperty getOperationResponseProperty() {
            return operationResponseProperty;
        }

        public static class Builder {

            private String valueName;
            private OperationResponseProperty operationResponseProperty;

            @Nonnull
            public static Builder providedValue() {
                return new Builder();
            }

            private Builder() {
            }

            @Nonnull
            public Builder withValueName(@Nonnull String valueName) {
                this.valueName = valueName;
                return this;
            }

            @Nonnull
            public Builder withOperationResponseProperty(@Nonnull OperationResponseProperty operationResponseProperty) {
                this.operationResponseProperty = operationResponseProperty;
                return this;
            }

            @Nonnull
            public ProvidedValue build() {
                return new ProvidedValue(
                    valueName,
                    operationResponseProperty
                );
            }

        }

    }

    public static class OperationResponseProperty {

        private final String operationId;
        private final String responsePropertyName;

        public OperationResponseProperty(String operationId, String responsePropertyName) {
            this.operationId = operationId;
            this.responsePropertyName = responsePropertyName;
        }

        public String getOperationId() {
            return operationId;
        }

        public String getResponsePropertyName() {
            return responsePropertyName;
        }

        public static class Builder {

            private String operationId;
            private String responsePropertyName;

            @Nonnull
            public static Builder operationResponseProperty() {
                return new Builder();
            }

            private Builder() {
            }

            @Nonnull
            public Builder withOperationId(@Nonnull String operationId) {
                this.operationId = operationId;
                return this;
            }

            @Nonnull
            public Builder withResponsePropertyName(@Nonnull String responsePropertyName) {
                this.responsePropertyName = responsePropertyName;
                return this;
            }

            @Nonnull
            public OperationResponseProperty build() {
                return new OperationResponseProperty(
                    operationId,
                    responsePropertyName
                );
            }

        }

    }

}

