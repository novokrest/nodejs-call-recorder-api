package com.nodejs.codegen.operation;

import com.nodejs.codegen.extensions.CodegenOperationEx;

public class OperationPrefixEnhancer implements OperationsEnhancer {

    @Override
    public void enhance(OperationsContext operationsContext) {
        operationsContext.getOperations().forEach(operation -> {
            String prefixPath = operation.path.equals("/update_profile_img")
                                ? "/upload"
                                : "/rapi";
            CodegenOperationEx.setPrefixPath(operation, prefixPath);
        });

    }
}
