package com.nodejs.codegen.operation;

import com.google.common.collect.ImmutableList;
import com.nodejs.codegen.dsl.ConstantStatement;
import com.nodejs.codegen.dsl.LambdaExpression;
import com.nodejs.codegen.dsl.Renderer;
import com.nodejs.codegen.dsl.Statement;
import com.nodejs.codegen.extensions.CodegenModelEx;
import com.nodejs.codegen.extensions.CodegenOperationEx;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import com.nodejs.codegen.value.ExpectResponseValueProvider;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenOperation;

import java.util.List;
import java.util.stream.Collectors;

import static com.nodejs.codegen.dsl.FunctionInvocationStatement.Builder.functionInvocationStatement;
import static com.nodejs.codegen.dsl.LambdaExpression.Builder.lambdaExpression;
import static com.nodejs.codegen.dsl.OperationCallStatement.Builder.operationCallStatement;
import static java.util.Collections.emptyList;

public class TestLambdaDefinitionFactory {

    private static final String CALLBACK_PARAMETER_NAME = "done";
    private static final String RESPONSE_PARAMETER_NAME = "response";

    private final OperationsContext operationsContext;
    private final ExpectResponseValueProvider expectResponseValueProvider;

    public TestLambdaDefinitionFactory(OperationsContext operationsContext, ExpectResponseValueProvider expectResponseValueProvider) {
        this.operationsContext = operationsContext;
        this.expectResponseValueProvider = expectResponseValueProvider;
    }

    public String getTestLambdaDefinition(String operationId) {
        LambdaExpression lambda = lambdaExpression()
            .withInputParams(CALLBACK_PARAMETER_NAME)
            .withStatements(buildOperationCallStatement(operationId))
            .build();
        Renderer renderer = new Renderer();
        lambda.render(renderer);
        return renderer.getResult();
    }

    private Statement buildOperationCallStatement(String operationId) {
        CodegenOperation operation = operationsContext.getOperationById(operationId);
        CodegenModel operationRequestModel = CodegenOperationEx.getOperationRequestModel(operation);
        Statement operationCallStatement = operationCallStatement()
            .withOperation(operation)
            .withRequest(operationRequestModel)
            .withCallback(lambdaExpression()
                .withInputParams(RESPONSE_PARAMETER_NAME)
                .withStatements(buildExpectStatements(CodegenOperationEx.getOperationResponseModel(operation), RESPONSE_PARAMETER_NAME))
                .build())
            .build();
        if (CodegenModelEx.getHasExtParams(operationRequestModel)) {
            return functionInvocationStatement()
                .withFunctionName(CodegenModelEx.getExtParamFunctionProviderName(operationRequestModel))
                .withArguments(lambdaExpression()
                    .withInputParams(CodegenModelEx.getExtParamNames(operationRequestModel).values().stream().sorted().collect(Collectors.toList()))
                    .withStatements(operationCallStatement)
                    .build())
                .build();
        } else {
            return operationCallStatement;
        }
    }

    private List<Statement> buildExpectStatements(CodegenModel operationResponseModel, String responseParameterName) {
        return ImmutableList.<Statement>builder()
            .add(new ConstantStatement("expect(response).toBeDefined()"))
            .addAll(
                operationResponseModel.vars.stream()
                    .filter(CodegenPropertyEx::getShouldExpect)
                    .map(property -> expectResponseValueProvider.getExpectStatement(operationResponseModel, property, responseParameterName))
                    .map(ConstantStatement::new)
                    .collect(Collectors.toList())
            )
            .add(functionInvocationStatement()
                .withFunctionName(CALLBACK_PARAMETER_NAME)
                .withArguments(emptyList())
                .build())
            .build();
    }

}
