package com.nodejs.codegen.operation;

import com.nodejs.codegen.value.ExpectResponseValueProvider;
import io.swagger.codegen.v3.CodegenOperation;

public class TestLambdaOperationEnhancer implements OperationsEnhancer {

    @Override
    public void enhance(OperationsContext operationsContext) {
        TestLambdaDefinitionFactory factory = new TestLambdaDefinitionFactory(operationsContext, new ExpectResponseValueProvider());
        operationsContext.getOperations().forEach(operation -> enhanceOperation(factory, operation));
    }

    private void enhanceOperation(TestLambdaDefinitionFactory factory, CodegenOperation operation) {
        operation.vendorExtensions.put("x-test-lambda", factory.getTestLambdaDefinition(operation.operationId));
    }

}
