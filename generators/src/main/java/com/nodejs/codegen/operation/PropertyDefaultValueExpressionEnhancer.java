package com.nodejs.codegen.operation;

import com.nodejs.codegen.core.WellKnown;
import com.nodejs.codegen.dsl.ConstantExpression;
import com.nodejs.codegen.dsl.DefaultModelExpression;
import com.nodejs.codegen.extensions.CodegenModelEx;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

import java.util.Map;

public class PropertyDefaultValueExpressionEnhancer implements OperationsEnhancer {

    @Override
    public void enhance(OperationsContext operationsContext) {
        setDefaultConstantExpressionForAllProperties(operationsContext);
        hackCreateFileRequestModel(operationsContext);
        hackUpdateProfileRequestModel(operationsContext);
    }

    private void setDefaultConstantExpressionForAllProperties(OperationsContext operationsContext) {
        operationsContext.getModels().values().forEach(this::setDefaultConstantExpressionForAllProperties);
    }

    private void setDefaultConstantExpressionForAllProperties(CodegenModel model) {
        Map<String, String> providedRequestField = CodegenModelEx.getExtParamNames(model);
        model.vars.forEach(property ->
            CodegenPropertyEx.setDefaultValueExpression(
                property,
                new ConstantExpression(chooseDefaultValue(providedRequestField, property))
        ));
    }

    private String chooseDefaultValue(Map<String, String> providedRequestField, CodegenProperty property) {
        if (!providedRequestField.keySet().contains(property.name)) {
            return property.defaultValue;
        }
        String inputParamName = providedRequestField.get(property.name);
        if (CodegenPropertyEx.isArray(property)) {
            return String.format("[%s]", inputParamName);
        }
        return inputParamName;
    }

    private void hackCreateFileRequestModel(OperationsContext operationsContext) {
        hackRequestModelWithDataFieldModel(operationsContext, WellKnown.CREATE_FILE_REQUEST_MODEL_NAME, WellKnown.CREATE_FILE_DATA_MODEL_NAME);
    }

    private void hackUpdateProfileRequestModel(OperationsContext operationsContext) {
        hackRequestModelWithDataFieldModel(operationsContext, WellKnown.UPDATE_PROFILE_REQUEST_MODEL_NAME, WellKnown.UPDATE_PROFILE_REQUEST_DATA_MODEL_NAME);
    }

    private void hackRequestModelWithDataFieldModel(OperationsContext operationsContext, String requestModelName, String requestDataFieldModelName) {
        CodegenModel requestModel = operationsContext.getModels().get(requestModelName);
        requestModel.vars.stream()
            .filter(property -> property.name.equals(WellKnown.COMMON_REQUEST_DATA_PROPERTY_NAME))
            .findFirst()
            .ifPresent(property -> CodegenPropertyEx.setDefaultValueExpression(
                property,
                new DefaultModelExpression(operationsContext.getModels().get(requestDataFieldModelName))
            ));
    }

}
