package com.nodejs.codegen.operation;

import com.google.common.collect.ImmutableList;
import com.nodejs.codegen.core.Names;
import com.nodejs.codegen.dsl.*;
import com.nodejs.codegen.extensions.CodegenOperationEx;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenOperation;
import io.swagger.codegen.v3.CodegenProperty;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static com.nodejs.codegen.dsl.FunctionInvocationStatement.Builder.functionInvocationStatement;
import static com.nodejs.codegen.dsl.LambdaExpression.Builder.lambdaExpression;
import static com.nodejs.codegen.dsl.ObjectFieldAccess.Builder.objectFieldAccess;
import static com.nodejs.codegen.dsl.OperationCallStatement.Builder.operationCallStatement;
import static com.nodejs.codegen.dsl.VarDefinition.Builder.varDefinition;
import static com.nodejs.codegen.operation.ProvideValuesFunctionDefinition.Builder.provideValuesFunctionDefinition;
import static com.nodejs.codegen.utils.StringUtils.toCamelCase;
import static com.nodejs.codegen.utils.StringUtils.toPascalCase;
import static java.util.Collections.singletonList;

public class ProvideValuesFunctionDefinitionFactory {

    private static final String CALLBACK_PARAMETER_NAME = "callback";

    private final OperationsContext operationsContext;

    public ProvideValuesFunctionDefinitionFactory(OperationsContext operationsContext) {
        this.operationsContext = operationsContext;
    }

    public ProvideValuesFunctionDefinition getDefinitionForSequentialOperationInvocations(
        ProvideValuesBySequentialOperationCallsFunctionDescription description
    ) {
        String functionName = Names.getProvideValuesFunctionName(description.getProvidedValues().stream()
            .map(ProvideValuesBySequentialOperationCallsFunctionDescription.ProvidedValue::getValueName)
            .collect(Collectors.toList()));
        String functionBody = buildFunctionBody(description);
        return provideValuesFunctionDefinition()
            .withProvidedValueNames(functionName)
            .withFunctionBody(functionBody)
            .build();
    }

    private String buildFunctionBody(ProvideValuesBySequentialOperationCallsFunctionDescription description) {
        if (description.getOperationCalls().isEmpty()) {
            throw new IllegalArgumentException("There must be at least one operation call: description=" + description);
        }
        LambdaExpression lambda = buildLambda(description);
        Renderer renderer = new Renderer();
        lambda.render(renderer);
        return renderer.getResult();
    }

    private LambdaExpression buildLambda(ProvideValuesBySequentialOperationCallsFunctionDescription description) {
        Iterator<ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall> operationCallIterator = description.getOperationCalls().iterator();
        return lambdaExpression()
            .withInputParams(ImmutableList.<String>builder()
                .addAll(description.getInputParameters())
                .add(CALLBACK_PARAMETER_NAME)
                .build())
            .withStatements(buildOperationCallStatement(operationCallIterator, description.getProvidedValues()))
            .build();
    }

    private List<Statement> buildOperationCallStatement(
        Iterator<ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall> operationCallIterator,
        List<ProvideValuesBySequentialOperationCallsFunctionDescription.ProvidedValue> providedValues
    ) {
        if (!operationCallIterator.hasNext()) {
            // last operation body: need to call callback
            return ImmutableList.<Statement>builder()
                .addAll(
                    providedValues.stream()
                        .map(providedValue -> varDefinition()
                            .withVarName(providedValue.getValueName())
                            .withExpression(
                                new VarExpression(getOperationResponseCachedFieldVariableName(providedValue.getOperationResponseProperty()))
                            )
                            .build())
                        .collect(Collectors.toList())
                )
                .add(
                    functionInvocationStatement()
                        .withFunctionName(CALLBACK_PARAMETER_NAME)
                        .withArguments(providedValues.stream()
                            .map(ProvideValuesBySequentialOperationCallsFunctionDescription.ProvidedValue::getValueName)
                            .map(ConstantExpression::new)
                            .collect(Collectors.toList()))
                        .build()
                )
                .build();
        }
        ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall operationCall = operationCallIterator.next();
        CodegenOperation operation = operationsContext.getOperationById(operationCall.getOperationId());
        String responseParamName = getOperationResponseParamName(operation.operationId);
        return singletonList(operationCallStatement()
            .withOperation(operation)
            .withRequest(buildOperationRequestModel(operation, operationCall))
            .withCallback(lambdaExpression()
                .withInputParams(responseParamName)
                .withStatements(buildOperationCallbackStatements(operation, operationCall, operationCallIterator, providedValues))
                .build())
            .build());
    }

    private CodegenModel buildOperationRequestModel(CodegenOperation operation, ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall operationCall) {
        CodegenModel originalRequestModel = CodegenOperationEx.getOperationRequestModel(operation);

        CodegenModel operationRequestModel = new CodegenModel();
        operationRequestModel.setClassname(originalRequestModel.classname);
        operationRequestModel.setVendorExtensions(originalRequestModel.vendorExtensions);

        List<CodegenProperty> operationRequestVars = originalRequestModel.vars.stream()
            .map(originalProperty -> {
                CodegenProperty property = new CodegenProperty();
                property.setName(originalProperty.name);
                property.setVendorExtensions(originalProperty.vendorExtensions);
                property.setDefaultValue(operationCall.getInputRequestParameterNames().contains(property.name)
                                         ? getOperationResponseCachedFieldVariableName(operationCall.getInputRequestParameters().get(property.name))
                                         : originalProperty.defaultValue);
                CodegenPropertyEx.setDefaultValueExpression(property, new ConstantExpression(property.defaultValue));

                return property;
            })
            .collect(Collectors.toList());
        operationRequestModel.setVars(operationRequestVars);

        return operationRequestModel;
    }

    private List<Statement> buildOperationCallbackStatements(
        CodegenOperation operation,
        ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall operationCall,
        Iterator<ProvideValuesBySequentialOperationCallsFunctionDescription.OperationCall> operationCallIterator,
        List<ProvideValuesBySequentialOperationCallsFunctionDescription.ProvidedValue> providedValues
    ) {
        return ImmutableList.<Statement>builder()
            .addAll(operationCall.getCachedResponsePropertyNames().stream()
                .map(responsePropertyName -> varDefinition()
                    .withVarName(getOperationResponseCachedFieldVariableName(operation.operationId, responsePropertyName))
                    .withExpression(objectFieldAccess()
                        .withObjectVarName(getOperationResponseParamName(operation.operationId))
                        .withFieldName(toCamelCase(responsePropertyName))
                        .build())
                    .build())
                .collect(Collectors.toList()))
            .addAll(buildOperationCallStatement(operationCallIterator, providedValues))
            .build();
    }


    private static String getOperationResponseCachedFieldVariableName(
        ProvideValuesBySequentialOperationCallsFunctionDescription.OperationResponseProperty operationResponseProperty
    ) {
        return getOperationResponseCachedFieldVariableName(operationResponseProperty.getOperationId(), operationResponseProperty.getResponsePropertyName());
    }

    private static String getOperationResponseCachedFieldVariableName(String operationId, String responsePropertyName) {
        return getOperationResponseParamName(operationId) + toPascalCase(responsePropertyName);
    }

    private static String getOperationResponseParamName(String operationId) {
        return toCamelCase(operationId) + "Response";
    }

}
