package com.nodejs.codegen.property;

import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

public interface PropertyEnhancer {

    void enhance(CodegenModel model, CodegenProperty property);

    default boolean canEnhance(CodegenModel model, CodegenProperty property) {
        return true;
    }

}
