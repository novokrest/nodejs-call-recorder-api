package com.nodejs.codegen.property;

import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

public class NameInCamelCasePropertyEnhancer implements PropertyEnhancer {

    @Override
    public void enhance(CodegenModel model, CodegenProperty property) {
        property.vendorExtensions.put("x-name-in-camel-case", pascalToCamelCase(property.nameInCamelCase));
    }

    private static String pascalToCamelCase(String str) {
        return Character.toLowerCase(str.charAt(0)) + str.substring(1);
    }

}
