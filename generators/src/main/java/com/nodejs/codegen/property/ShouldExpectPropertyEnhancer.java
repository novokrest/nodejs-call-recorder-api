package com.nodejs.codegen.property;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.nodejs.codegen.core.WellKnown;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

import java.util.Map;
import java.util.Set;

import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;

public class ShouldExpectPropertyEnhancer implements PropertyEnhancer {

    private static final Map<String, Set<String>> SHOULD_NOT_EXPECT_TYPE_PROPERTIES = ImmutableMap.<String, Set<String>>builder()
        .put(WellKnown.GET_LANGUAGES_RESPONSE_MODEL_NAME, singleton(WellKnown.COMMON_RESPONSE_MSG_PROPERTY_NAME))
        .put(WellKnown.GET_TRANSLATIONS_RESPONSE_MODEL_NAME, ImmutableSet.of(WellKnown.COMMON_RESPONSE_MSG_PROPERTY_NAME, WellKnown.COMMON_RESPONSE_CODE_PROPERTY_NAME))
        .build();

    @Override
    public void enhance(CodegenModel model, CodegenProperty property) {
        boolean shouldNotExpect = SHOULD_NOT_EXPECT_TYPE_PROPERTIES.getOrDefault(model.classname, emptySet()).contains(property.name);
        CodegenPropertyEx.setShouldExpect(property, !shouldNotExpect);
    }

}
