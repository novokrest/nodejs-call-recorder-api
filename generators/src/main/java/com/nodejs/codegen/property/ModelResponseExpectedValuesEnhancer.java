package com.nodejs.codegen.property;

import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

import java.util.Optional;

import static com.nodejs.codegen.utils.StringUtils.wrapWithSingleQuotes;

public class ModelResponseExpectedValuesEnhancer implements PropertyEnhancer {

    @Override
    public void enhance(CodegenModel model, CodegenProperty property) {
        Optional.ofNullable(getExpectedValue(model, property)).ifPresent(value -> {
            CodegenPropertyEx.setExpectedValue(property, value.toString());
        });
    }

    private Object getExpectedValue(CodegenModel model, CodegenProperty property) {
        switch (property.name) {
            case "status":
                return wrapWithSingleQuotes( "ok");
            case "credits":
            case "credits_trans":
                return 0;
            default:
                return null;
        }
    }

    @Override
    public boolean canEnhance(CodegenModel model, CodegenProperty property) {
        return model.name.endsWith("Response");
    }
}
