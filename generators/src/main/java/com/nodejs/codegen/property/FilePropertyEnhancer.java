package com.nodejs.codegen.property;

import com.google.common.collect.ImmutableMap;
import com.nodejs.codegen.core.WellKnown;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;

public class FilePropertyEnhancer implements PropertyEnhancer {

    private static final Map<String, String> FILE_TYPE_PROPERTIES = ImmutableMap.<String, String>builder()
        .put(WellKnown.CREATE_FILE_REQUEST_MODEL_NAME, WellKnown.COMMON_REQUEST_FILE_PROPERTY_NAME)
        .put(WellKnown.UPLOAD_META_FILE_REQUEST_MODEL_NAME, WellKnown.COMMON_REQUEST_FILE_PROPERTY_NAME)
        .put(WellKnown.UPDATE_PROFILE_IMAGE_REQUEST_MODEL_NAME, WellKnown.COMMON_REQUEST_FILE_PROPERTY_NAME)
        .build();

    @Override
    public void enhance(CodegenModel model, CodegenProperty property) {
        if (FILE_TYPE_PROPERTIES.getOrDefault(model.classname, StringUtils.EMPTY).equals(property.name)) {
            CodegenPropertyEx.setFile(property, true);
        }
    }

}
