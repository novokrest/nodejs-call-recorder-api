package com.nodejs.codegen.property;

import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

public class ArrayPropertyEnhancer implements PropertyEnhancer {

    @Override
    public void enhance(CodegenModel model, CodegenProperty property) {
        if (CodegenPropertyEx.isArray(property)) {
            CodegenPropertyEx.setArray(property, true);
        }
    }

}



