package com.nodejs.codegen.property;

import com.nodejs.codegen.value.DefaultValuesProvider;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

public class DefaultValuePropertyEnhancer implements PropertyEnhancer {

    private final DefaultValuesProvider defaultValuesProvider = new DefaultValuesProvider();

    @Override
    public void enhance(CodegenModel model, CodegenProperty property) {
        property.setDefaultValue(defaultValuesProvider.getDefaultValue(model, property));
    }

}
