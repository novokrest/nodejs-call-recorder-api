package com.nodejs.codegen.dsl;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class VarDefinition implements Statement {

    private final String varName;

    private final Expression expression;

    private VarDefinition(
        @Nonnull String varName,
        @Nonnull Expression expression
    ) {
        this.varName = requireNonNull(varName, "varName");
        this.expression = requireNonNull(expression, "expression");
    }

    @Nonnull
    public String getVarName() {
        return varName;
    }

    @Nonnull
    public Expression getExpression() {
        return expression;
    }


    @Override
    public void render(Renderer renderer) {
        renderer.renderVarDefinition(this);
    }

    @Nonnull
    @Override
    public String toString() {
        return "VarDefinition{" +
            "varName='" + varName + '\'' +
            ", expression=" + expression +
            '}';
    }

    public static class Builder {

        private String varName;
        private Expression expression;

        @Nonnull
        public static Builder varDefinition() {
            return new Builder();
        }

        private Builder() {
        }

        @Nonnull
        public Builder withVarName(@Nonnull String varName) {
            this.varName = varName;
            return this;
        }

        @Nonnull
        public Builder withExpression(@Nonnull Expression expression) {
            this.expression = expression;
            return this;
        }

        @Nonnull
        public VarDefinition build() {
            return new VarDefinition(
                varName,
                expression
            );
        }

    }

}
