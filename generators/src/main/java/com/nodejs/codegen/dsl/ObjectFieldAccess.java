package com.nodejs.codegen.dsl;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class ObjectFieldAccess implements Expression {

    private final String objectVarName;

    private final String fieldName;

    private ObjectFieldAccess(
        @Nonnull String objectVarName,
        @Nonnull String fieldName
    ) {
        this.objectVarName = requireNonNull(objectVarName, "objectVarName");
        this.fieldName = requireNonNull(fieldName, "fieldName");
    }

    @Nonnull
    public String getObjectVarName() {
        return objectVarName;
    }

    @Nonnull
    public String getFieldName() {
        return fieldName;
    }


    @Override
    public void render(Renderer renderer) {
        renderer.renderObjectFieldAccess(this);
    }

    @Nonnull
    @Override
    public String toString() {
        return "ObjectFieldAccess{" +
            "objectVarName='" + objectVarName + '\'' +
            ", fieldName='" + fieldName + '\'' +
            '}';
    }

    public static class Builder {

        private String objectVarName;
        private String fieldName;

        @Nonnull
        public static Builder objectFieldAccess() {
            return new Builder();
        }

        private Builder() {
        }

        @Nonnull
        public Builder withObjectVarName(@Nonnull String objectVarName) {
            this.objectVarName = objectVarName;
            return this;
        }

        @Nonnull
        public Builder withFieldName(@Nonnull String fieldName) {
            this.fieldName = fieldName;
            return this;
        }

        @Nonnull
        public ObjectFieldAccess build() {
            return new ObjectFieldAccess(
                objectVarName,
                fieldName
            );
        }

    }

}

