package com.nodejs.codegen.dsl;

public interface Renderable {

    void render(Renderer renderer);

}
