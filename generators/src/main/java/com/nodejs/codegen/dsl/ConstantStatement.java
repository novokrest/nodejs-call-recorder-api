package com.nodejs.codegen.dsl;

public class ConstantStatement implements Statement {

    private final Object value;

    public ConstantStatement(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public void render(Renderer renderer) {
        renderer.renderConstantStatement(this);
    }

}
