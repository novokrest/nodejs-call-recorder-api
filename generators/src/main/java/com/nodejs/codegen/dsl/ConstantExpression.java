package com.nodejs.codegen.dsl;

public class ConstantExpression implements Expression {

    private final Object value;

    public ConstantExpression(Object value) {
        this.value = value;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public void render(Renderer renderer) {
        renderer.renderConstantExpression(this);
    }

}
