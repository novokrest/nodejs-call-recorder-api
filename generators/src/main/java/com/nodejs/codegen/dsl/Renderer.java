package com.nodejs.codegen.dsl;

import com.nodejs.codegen.extensions.CodegenOperationEx;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import org.apache.commons.lang3.StringUtils;

import static com.nodejs.codegen.utils.StringUtils.toCamelCase;

public class Renderer {

    private final StringBuilder output = new StringBuilder();
    private int currentOffset;

    public String getResult() {
        return output.toString();
    }

    public void renderLambda(LambdaExpression lambda) {
        if (lambda.getInputParams().size() == 1) {
            append(lambda.getInputParams().get(0) + " => {");
        } else {
            append("(" + String.join(", ", lambda.getInputParams()) + ") => {");
        }
        appendNewLine();
        increaseOffset();
        lambda.getStatements().forEach(statement -> statement.render(this));
        decreaseOffset();
        appendWithOffset("}");
    }

    public void renderVarDefinition(VarDefinition var) {
        appendWithOffset("const " + var.getVarName() + " = ");
        var.getExpression().render(this);
        append(";");
        appendNewLine();
    }

    public void renderFunctionInvocation(FunctionInvocationStatement functionInvocation) {
        appendWithOffset(functionInvocation.getFunctionName() + "(");
        for (int i = 0; i < functionInvocation.getArguments().size(); i++) {
            functionInvocation.getArguments().get(i).render(this);
            boolean isNotLast = i != functionInvocation.getArguments().size() - 1;
            if (isNotLast) {
                append(", ");
            }
        }
        append(");");
        appendNewLine();
    }

    public void renderOperationCall(OperationCallStatement operationCall) {
        appendWithOffset("api." + CodegenOperationEx.getOperationName(operationCall.getOperation())+ "(");
        appendNewLine();
        increaseOffset();
        appendWithOffset("");
        renderModelBuild(operationCall.getRequest());
        append(",");
        appendNewLine();
        appendWithOffset("");
        operationCall.getCallback().render(this);
        decreaseOffset();
        append(");");
        appendNewLine();
    }

    private void renderModelBuild(CodegenModel model) {
        append(model.classname + ".builder()");
        appendNewLine();
        increaseOffset();
        model.vars.forEach(property -> {
            appendWithOffset(String.format(".%s(", toCamelCase(property.name)));
            CodegenPropertyEx.getDefaultValueExpression(property).render(this);
            append(")");
            appendNewLine();
        });
        appendWithOffset(".build()");
        decreaseOffset();
    }

    public void renderObjectFieldAccess(ObjectFieldAccess objectFieldAccess) {
        append(objectFieldAccess.getObjectVarName() + "." + objectFieldAccess.getFieldName());
    }

    public void renderVarExpression(VarExpression varExpression) {
        append(varExpression.getVarName());
    }

    public void renderConstantExpression(ConstantExpression constant) {
        append(constant.getValue().toString());
    }

    public void renderConstantStatement(ConstantStatement constantStatement) {
        appendWithOffset(constantStatement.getValue().toString());
        append(";");
        appendNewLine();
    }

    public void renderDefaultModelExpression(DefaultModelExpression modelExpression) {
        renderModelBuild(modelExpression.getValue());
    }

    private void appendWithOffset(String str) {
        output.append(offset()).append(str);
    }

    private void append(String str) {
        output.append(str);
    }

    private String offset() {
        return StringUtils.repeat('\t', currentOffset);
    }

    private void appendNewLine() {
        output.append(System.lineSeparator());
    }

    private void increaseOffset() {
        ++currentOffset;
    }

    private void decreaseOffset() {
        if (--currentOffset < 0) {
            throw new IllegalStateException("Offset could not be negative");
        }
    }

}
