package com.nodejs.codegen.dsl;

import javax.annotation.Nonnull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

public class LambdaExpression implements Expression {

    private final List<String> inputParams;

    private final List<Statement> statements;

    public LambdaExpression(List<String> inputParams, List<Statement> statements) {
        this.inputParams = inputParams;
        this.statements = statements;
    }

    public List<String> getInputParams() {
        return inputParams;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    @Override
    public void render(Renderer renderer) {
        renderer.renderLambda(this);
    }

    public static class Builder {

        private List<String> inputParams;
        private List<Statement> statements;

        @Nonnull
        public static Builder lambdaExpression() {
            return new Builder();
        }

        private Builder() {
        }

        @Nonnull
        public Builder withInputParams(@Nonnull String inputParam) {
            this.inputParams = singletonList(inputParam);
            return this;
        }

        @Nonnull
        public Builder withInputParams(@Nonnull List<String> inputParams) {
            this.inputParams = inputParams;
            return this;
        }

        @Nonnull
        public Builder withStatements(@Nonnull Statement statement) {
            this.statements = singletonList(statement);
            return this;
        }

        @Nonnull
        public Builder withStatements(@Nonnull Statement... statements) {
            this.statements = Arrays.stream(statements).collect(Collectors.toList());
            return this;
        }

        @Nonnull
        public Builder withStatements(@Nonnull List<Statement> statements) {
            this.statements = statements;
            return this;
        }

        @Nonnull
        public LambdaExpression build() {
            return new LambdaExpression(
                inputParams,
                statements
            );
        }

    }

}
