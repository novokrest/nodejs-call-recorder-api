package com.nodejs.codegen.dsl;

/**
 * TODO:
 */
public class VarExpression implements Expression {

    private final String varName;

    public VarExpression(String varName) {
        this.varName = varName;
    }

    public String getVarName() {
        return varName;
    }

    @Override
    public void render(Renderer renderer) {
        renderer.renderVarExpression(this);
    }
}
