package com.nodejs.codegen.dsl;

import io.swagger.codegen.v3.CodegenModel;

public class DefaultModelExpression implements Expression {

    private final CodegenModel value;

    public DefaultModelExpression(CodegenModel value) {
        this.value = value;
    }

    public CodegenModel getValue() {
        return value;
    }

    @Override
    public void render(Renderer renderer) {
        renderer.renderDefaultModelExpression(this);
    }

}
