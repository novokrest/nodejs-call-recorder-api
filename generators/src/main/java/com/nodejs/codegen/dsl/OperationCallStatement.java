package com.nodejs.codegen.dsl;

import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenOperation;

import javax.annotation.Nonnull;

import static java.util.Objects.requireNonNull;

public class OperationCallStatement implements Statement {

    private final CodegenOperation operation;
    private final CodegenModel request;
    private final LambdaExpression callback;

    private OperationCallStatement(
        @Nonnull CodegenOperation operation,
        @Nonnull CodegenModel request,
        @Nonnull LambdaExpression callback
    ) {
        this.operation = requireNonNull(operation, "operation");
        this.request = requireNonNull(request, "request");
        this.callback = requireNonNull(callback, "callback");
    }

    @Nonnull
    public CodegenOperation getOperation() {
        return operation;
    }

    @Nonnull
    public CodegenModel getRequest() {
        return request;
    }

    public LambdaExpression getCallback() {
        return callback;
    }

    @Nonnull
    @Override
    public String toString() {
        return "OperationCallStatement{" +
            "operation=" + operation +
            ", request=" + request +
            ", callback=" + callback +
            '}';
    }

    @Override
    public void render(Renderer renderer) {
        renderer.renderOperationCall(this);
    }

    public static class Builder {

        private CodegenOperation operation;
        private CodegenModel request;
        private LambdaExpression callback;

        public static Builder operationCallStatement() {
            return new Builder();
        }

        private Builder() {
        }

        @Nonnull
        public Builder withOperation(@Nonnull CodegenOperation operation) {
            this.operation = operation;
            return this;
        }

        @Nonnull
        public Builder withRequest(@Nonnull CodegenModel request) {
            this.request = request;
            return this;
        }

        @Nonnull
        public Builder withCallback(@Nonnull LambdaExpression callback) {
            this.callback = callback;
            return this;
        }

        @Nonnull
        public OperationCallStatement build() {
            return new OperationCallStatement(
                operation,
                request,
                callback
            );
        }

    }

}
