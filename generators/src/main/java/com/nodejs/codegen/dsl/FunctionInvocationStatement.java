package com.nodejs.codegen.dsl;

import javax.annotation.Nonnull;
import java.util.List;

import static java.util.Collections.singletonList;
import static java.util.Objects.requireNonNull;

public class FunctionInvocationStatement implements Statement {

    private final String functionName;

    private final List<Expression> arguments;

    private FunctionInvocationStatement(
        @Nonnull String functionName,
        @Nonnull List<Expression> arguments
    ) {
        this.functionName = requireNonNull(functionName, "functionName");
        this.arguments = requireNonNull(arguments, "arguments");
    }

    @Nonnull
    public String getFunctionName() {
        return functionName;
    }

    @Nonnull
    public List<Expression> getArguments() {
        return arguments;
    }

    @Override
    public void render(Renderer renderer) {
        renderer.renderFunctionInvocation(this);
    }

    @Nonnull
    @Override
    public String toString() {
        return "FunctionInvocationStatement{" +
            "functionName='" + functionName + '\'' +
            ", arguments=" + arguments +
            '}';
    }

    public static class Builder {

        private String functionName;
        private List<Expression> arguments;

        @Nonnull
        public static Builder functionInvocationStatement() {
            return new Builder();
        }

        private Builder() {
        }

        @Nonnull
        public Builder withFunctionName(@Nonnull String functionName) {
            this.functionName = functionName;
            return this;
        }

        @Nonnull
        public Builder withArguments(@Nonnull Expression argument) {
            this.arguments = singletonList(argument);
            return this;
        }

        @Nonnull
        public Builder withArguments(@Nonnull List<Expression> arguments) {
            this.arguments = arguments;
            return this;
        }

        @Nonnull
        public FunctionInvocationStatement build() {
            return new FunctionInvocationStatement(
                functionName,
                arguments
            );
        }

    }

}

