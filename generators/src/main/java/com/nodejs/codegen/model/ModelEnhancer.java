package com.nodejs.codegen.model;

import io.swagger.codegen.v3.CodegenModel;

public interface ModelEnhancer {

    void enhance(CodegenModel model);

}
