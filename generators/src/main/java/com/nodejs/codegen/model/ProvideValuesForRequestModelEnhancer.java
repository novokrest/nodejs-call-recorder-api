package com.nodejs.codegen.model;

import com.nodejs.codegen.core.Names;
import com.nodejs.codegen.core.WellKnown;
import com.nodejs.codegen.extensions.CodegenModelEx;
import com.nodejs.codegen.value.GlobalProvidedValues;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

import java.util.HashMap;
import java.util.Map;

public class ProvideValuesForRequestModelEnhancer implements ModelEnhancer {

    @Override
    public void enhance(CodegenModel model) {
        Map<String, String> extParams = new HashMap<>();
        if (hasApiKeyPropertyName(model)) {
            extParams.put(WellKnown.COMMON_REQUEST_API_KEY_PROPERTY_NAME, GlobalProvidedValues.API_KEY.getValueName());
        }
        if (model.classname.equals(WellKnown.CLONE_FILE_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.CLONE_FILE_REQUEST_ID_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.DELETE_FILES_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.DELETE_FILES_REQUEST_IDS_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.DELETE_META_FILES_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.DELETE_META_FILES_REQUEST_IDS_PROPERTY_NAME, GlobalProvidedValues.META_FILE_ID.getValueName());
            extParams.put(WellKnown.DELETE_META_FILES_REQUEST_PARENT_ID_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.DELETE_FOLDER_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.DELETE_FOLDER_REQUEST_ID_PROPERTY_NAME, GlobalProvidedValues.FOLDER_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.UPLOAD_META_FILE_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.UPLOAD_META_FILE_REQUEST_PARENT_ID_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.GET_META_FILES_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.COMMON_REQUEST_PARENT_ID_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.RECOVER_FILE_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.COMMON_REQUEST_ID_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.UPDATE_FILE_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.COMMON_REQUEST_ID_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.UPDATE_FOLDER_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.COMMON_REQUEST_ID_PROPERTY_NAME, GlobalProvidedValues.FOLDER_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.UPDATE_STAR_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.COMMON_REQUEST_ID_PROPERTY_NAME, GlobalProvidedValues.FILE_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.VERIFY_FOLDER_PASS_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.COMMON_REQUEST_ID_PROPERTY_NAME, GlobalProvidedValues.FOLDER_ID.getValueName());
        }
        if (model.classname.equals(WellKnown.VERIFY_PHONE_REQUEST_MODEL_NAME)) {
            extParams.put(WellKnown.VERIFY_PHONE_REQUEST_CODE_PROPERTY_NAME, GlobalProvidedValues.CODE.getValueName());
        }

        enhanceModelToObtainProperty(model, extParams);
    }

    private static boolean hasApiKeyPropertyName(CodegenModel model) {
        return model.vars.stream()
            .map(CodegenProperty::getName)
            .anyMatch(propertyName -> propertyName.equals(WellKnown.COMMON_REQUEST_API_KEY_PROPERTY_NAME));
    }

    private void enhanceModelToObtainProperty(CodegenModel model, Map<String, String> extParams) {
        CodegenModelEx.setHasExtParams(model, !extParams.isEmpty());
        CodegenModelEx.setExtParams(model, extParams);
        CodegenModelEx.setExtParamFunctionProviderName(model, Names.getProvideValuesFunctionName(extParams.values()));
    }

}
