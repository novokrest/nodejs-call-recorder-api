package com.nodejs.codegen.model;

import com.nodejs.codegen.core.WellKnown;
import com.nodejs.codegen.extensions.CodegenModelEx;
import com.nodejs.codegen.extensions.CodegenOperationEx;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import com.nodejs.codegen.operation.OperationsContext;
import com.nodejs.codegen.operation.OperationsEnhancer;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static com.nodejs.codegen.utils.StringUtils.toCamelCase;

public class FormParamsModelEnhancer implements OperationsEnhancer {

    @Override
    public void enhance(OperationsContext operationsContext) {
        operationsContext.getOperations().stream()
            .map(CodegenOperationEx::getOperationRequestModel)
            .filter(requestModel -> !requestModel.classname.equals(WellKnown.UPDATE_PROFILE_REQUEST_MODEL_NAME))
            .forEach(this::enhanceRequestModel);
        enhanceUpdateProfileRequestModel(
            operationsContext.getModels().get(WellKnown.UPDATE_PROFILE_REQUEST_MODEL_NAME),
            operationsContext.getModels().get(WellKnown.UPDATE_PROFILE_REQUEST_DATA_MODEL_NAME)
        );
    }

    private void enhanceRequestModel(CodegenModel requestModel) {
        List<FormParam> formParams = new ArrayList<>(requestModel.vars.size());
        requestModel.vars.stream()
            .sorted(Comparator.comparing(CodegenProperty::getName))
            .map(property -> new FormParam(property.name, getPropertyFormValue(property)))
            .forEach(formParams::add);
        CodegenModelEx.setFormParams(requestModel, formParams);
    }

    private void enhanceUpdateProfileRequestModel(CodegenModel requestModel, CodegenModel requestDataModel) {
        List<FormParam> formParams = new ArrayList<>(requestModel.vars.size());
        requestModel.vars.stream()
            .filter(property -> !property.name.equals(WellKnown.COMMON_REQUEST_DATA_PROPERTY_NAME))
            .sorted(Comparator.comparing(CodegenProperty::getName))
            .map(property -> new FormParam(property.name, getPropertyFormValue(property)))
            .forEach(formParams::add);
        formParams.addAll(getDataPropertyFormParams(requestDataModel));
        CodegenModelEx.setFormParams(requestModel, formParams);
    }

    private String getPropertyFormValue(CodegenProperty property) {
        if (CodegenPropertyEx.getFile(property)) {
            return String.format("fs.createReadStream(request.%s)", toCamelCase(property.name));
        }
        if (CodegenPropertyEx.getComplex(property)) {
            return String.format("JSON.stringify(request.%s)", toCamelCase(property.name));
        }
        if (CodegenPropertyEx.isArray(property)) {
            return String.format("request.%s.join(',')", toCamelCase(property.name));
        }
        return String.format("request.%s", toCamelCase(property.name));
    }

    private List<FormParam> getDataPropertyFormParams(CodegenModel requestDataModel) {
        return requestDataModel.vars.stream()
            .map(property ->  new FormParam(
                String.format("data[%s]", property.name),
                String.format("request.data.%s", toCamelCase(property.name)))
            )
            .collect(Collectors.toList());
    }

    public static class FormParam {

        private final String name;
        private final String value;

        private FormParam(String name, String value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public String getValue() {
            return value;
        }
    }

}
