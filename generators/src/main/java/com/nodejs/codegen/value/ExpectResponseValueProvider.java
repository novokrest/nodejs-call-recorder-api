package com.nodejs.codegen.value;

import com.google.common.collect.ImmutableMap;
import com.nodejs.codegen.extensions.CodegenPropertyEx;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;

import static com.nodejs.codegen.utils.StringUtils.toCamelCase;

public class ExpectResponseValueProvider {

    private static final Map<Pair<String, String>, String> RESPONSE_FIELD_EXPECTED_VALUES = ImmutableMap.<Pair<String, String>, String>builder()
        .put(Pair.of("CreateFileResponse", "id"), "toBeGreaterThan(0)")
        .put(Pair.of("CloneFileResponse", "id"), "toBeGreaterThan(0)")
        .put(Pair.of("CreateFolderResponse", "id"), "toBeGreaterThan(0)")
        .put(Pair.of("UploadMetaFileResponse", "id"), "toBeGreaterThan(0)")
        .put(Pair.of("UploadMetaFileResponse", "parent_id"), String.format("toEqual(%s)", GlobalProvidedValues.FILE_ID.getValueName()))
        .put(Pair.of("GetFoldersResponse", "folders"), "toBeTruthy()")
        .put(Pair.of("GetFilesResponse", "files"), "toHaveLength(0)")
        .put(Pair.of("RegisterPhoneResponse", "msg"), "toContain('Verification Code Sent')")
        .put(Pair.of("UpdateFolderResponse", "msg"), "toContain('Folder Updated')")
        .put(Pair.of("VerifyFolderPassResponse", "msg"), "toContain('Correct')")
        .put(Pair.of("UpdateOrderResponse", "msg"), "toContain('Order Updated')")
        .put(Pair.of("UpdateProfileImgResponse", "msg"), "toContain('Profile Picture Updated')")
        .put(Pair.of("VerifyPhoneResponse", "msg"), "toContain('Phone Verified')")
        .put(Pair.of("UpdateProfileResponse", "msg"), "toContain('Profile Updated')")
        .build();

    private static final Map<String, String> RESPONSE_CODE_EXPECTED_VALUES = ImmutableMap.<String, String>builder()
        .put("CloneFileResponse", "file_cloned")
        .put("CreateFolderResponse", "folder_created")
        .build();

    public String getExpectStatement(CodegenModel model, CodegenProperty property, String responseParameterName) {
        if (RESPONSE_FIELD_EXPECTED_VALUES.containsKey(Pair.of(model.classname, property.name))) {
            return String.format("expect(%s.%s).%s", responseParameterName, toCamelCase(property.name), RESPONSE_FIELD_EXPECTED_VALUES.get(Pair.of(model.classname, property.name)));
        }
        if (property.name.equals("msg")) {
            return String.format("expect(%s.msg.toLowerCase()).toContain('success')", responseParameterName);
        }
        if (property.name.equals("code")) {
            if (RESPONSE_CODE_EXPECTED_VALUES.containsKey(model.classname)) {
                return String.format("expect(%s.code).toEqual('%s')", responseParameterName, RESPONSE_CODE_EXPECTED_VALUES.get(model.classname));
            } else {
                return String.format("expect(%s.code).toBeTruthy()", responseParameterName);
            }
        }
        String expectedValue = CodegenPropertyEx.getExpectedValue(property);
        if (expectedValue != null) {
            return String.format("expect(%s.%s).toEqual(%s)", responseParameterName, toCamelCase(property.name), expectedValue);
        }
        return String.format("expect(%s.%s).toBeDefined()", responseParameterName, toCamelCase(property.name));
    }

}
