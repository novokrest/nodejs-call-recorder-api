package com.nodejs.codegen.value;

import java.util.Arrays;

public enum GlobalProvidedValues {

    CODE("code"),

    API_KEY("apiKey"),

    FILE_ID("fileId"),

    META_FILE_ID("metaFileId"),

    FOLDER_ID("folderId"),

    ;

    static {
        if (values().length != Arrays.stream(values()).distinct().count()) {
            throw new IllegalStateException("All values should be unique");
        }
    }

    private final String valueName;

    GlobalProvidedValues(String valueName) {
        this.valueName = valueName;
    }

    public String getValueName() {
        return valueName;
    }

}
