package com.nodejs.codegen.value;

import com.google.common.collect.ImmutableMap;
import com.nodejs.codegen.core.WellKnown;
import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenProperty;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;

import java.util.Map;
import java.util.Optional;

import static com.nodejs.codegen.utils.StringUtils.wrapWithSingleQuotes;

public class DefaultValuesProvider {

    private static final String DEVICE_TOKEN = "871284c348e04a9cacab8aca6b2f3c9a";
    private static final int FILE_ID = 1;
    private static final String FILE_NAME = "test-file";

    private static final Map<String, Object> GLOBAL_DEFAULT_VALUES = ImmutableMap.<String, Object>builder()
        .put("token", wrapWithSingleQuotes("55942ee3894f51000530894"))
        .put("phone", wrapWithSingleQuotes("+16463742122"))
        .put("device_id", wrapWithSingleQuotes(DEVICE_TOKEN))
        .put("device_token", wrapWithSingleQuotes(DEVICE_TOKEN))
        .put("device_type", wrapWithSingleQuotes("ios"))
        .put("receipt", wrapWithSingleQuotes("test"))
        .put("amount", 100)
        .put("product_id", 1)
        .put("mcc", wrapWithSingleQuotes("300"))
        .put("app", wrapWithSingleQuotes("rec"))
        .put("time_zone", wrapWithSingleQuotes("10"))
        .put("timezone", wrapWithSingleQuotes("10"))
        .put("file", wrapWithSingleQuotes("test-file"))
        .put("pass", wrapWithSingleQuotes("12345"))
        .put("f_name", wrapWithSingleQuotes("f"))
        .put("l_name", wrapWithSingleQuotes("l"))
        .put("notes", wrapWithSingleQuotes("n"))
        .put("email", wrapWithSingleQuotes("e@mail.ru"))
        .put("tags", wrapWithSingleQuotes("t"))
        .put("name", wrapWithSingleQuotes("n"))
        .put("remind_days", wrapWithSingleQuotes("10"))
        .put("remind_date", wrapWithSingleQuotes("2019-09-03T21:11:51.824121+03:00"))
        .build();

    private static final Map<Pair<String, String>, Object> MODEL_PROPERTY_DEFAULT_VALUES = ImmutableMap.<Pair<String, String>, Object>builder()
        .put(Pair.of(WellKnown.CLONE_FILE_REQUEST_MODEL_NAME, "id"), FILE_ID)
        .put(Pair.of(WellKnown.CREATE_FILE_REQUEST_MODEL_NAME, WellKnown.CREATE_FILE_REQUEST_FILE_PROPERTY_NAME), wrapWithSingleQuotes("src/resources/audio.mp3"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "name"), wrapWithSingleQuotes(FILE_NAME))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "email"), wrapWithSingleQuotes("e@mail.com"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "l_name"), wrapWithSingleQuotes("l"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "f_name"), wrapWithSingleQuotes("f"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "notes"), wrapWithSingleQuotes("n"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "tags"), wrapWithSingleQuotes("t"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "meta"), wrapWithSingleQuotes("m"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "source"), wrapWithSingleQuotes("s"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "remind_days"), wrapWithSingleQuotes("10"))
        .put(Pair.of(WellKnown.CREATE_FILE_DATA_MODEL_NAME, "remind_date"), wrapWithSingleQuotes("2019-09-03T21:11:51.824121+03:00"))
        .put(Pair.of(WellKnown.DELETE_FILES_REQUEST_MODEL_NAME, "action"), wrapWithSingleQuotes("remove_forever"))
        .put(Pair.of(WellKnown.CREATE_FOLDER_REQUEST_MODEL_NAME, "name"), wrapWithSingleQuotes("test-folder"))
        .put(Pair.of(WellKnown.UPLOAD_META_FILE_REQUEST_MODEL_NAME, "name"), wrapWithSingleQuotes("test-meta"))
        .put(Pair.of(WellKnown.UPLOAD_META_FILE_REQUEST_MODEL_NAME, "id"), 1)
        .put(Pair.of(WellKnown.UPLOAD_META_FILE_REQUEST_MODEL_NAME, WellKnown.UPLOAD_META_FILE_REQUEST_FILE_PROPERTY_NAME), wrapWithSingleQuotes("src/resources/java.png"))
        .put(Pair.of(WellKnown.GET_FILES_REQUEST_MODEL_NAME, "page"), 0)
        .put(Pair.of(WellKnown.GET_FILES_REQUEST_MODEL_NAME, "folder_id"), 0)
        .put(Pair.of(WellKnown.GET_FILES_REQUEST_MODEL_NAME, "source"), wrapWithSingleQuotes("all"))
        .put(Pair.of(WellKnown.GET_FILES_REQUEST_MODEL_NAME, "reminder"), false)
        .put(Pair.of(WellKnown.GET_FILES_REQUEST_MODEL_NAME, "q"), wrapWithSingleQuotes("hello"))
        .put(Pair.of(WellKnown.GET_FILES_REQUEST_MODEL_NAME, "id"), 0)
        .put(Pair.of(WellKnown.GET_FILES_REQUEST_MODEL_NAME, "op"), wrapWithSingleQuotes("greater"))
        .put(Pair.of(WellKnown.GET_TRANSLATIONS_REQUEST_MODEL_NAME, "language"), wrapWithSingleQuotes("en_US"))
        .put(Pair.of(WellKnown.NOTIFY_USER_REQUEST_MODEL_NAME, "title"), wrapWithSingleQuotes("test-title"))
        .put(Pair.of(WellKnown.NOTIFY_USER_REQUEST_MODEL_NAME, "body"), wrapWithSingleQuotes("test-body"))
        .put(Pair.of(WellKnown.RECOVER_FILE_REQUEST_MODEL_NAME, WellKnown.RECOVER_FILE_REQUEST_FOLDER_ID_PROPERTY_NAME), 0)
        .put(Pair.of(WellKnown.UPDATE_STAR_REQUEST_MODEL_NAME, "star"), true)
        .put(Pair.of(WellKnown.UPDATE_STAR_REQUEST_MODEL_NAME, "type"), wrapWithSingleQuotes("file"))
        .put(Pair.of(WellKnown.UPDATE_SETTINGS_REQUEST_MODEL_NAME, "play_beep"), wrapWithSingleQuotes("no"))
        .put(Pair.of(WellKnown.UPDATE_SETTINGS_REQUEST_MODEL_NAME, "files_permission"), wrapWithSingleQuotes("private"))
        .put(Pair.of(WellKnown.UPDATE_PROFILE_IMAGE_REQUEST_MODEL_NAME, WellKnown.COMMON_REQUEST_FILE_PROPERTY_NAME), wrapWithSingleQuotes("src/resources/java.png"))
        .put(Pair.of(WellKnown.UPDATE_FILE_REQUEST_MODEL_NAME, "folder_id"), 0)
        .put(Pair.of(WellKnown.UPDATE_FOLDER_REQUEST_MODEL_NAME, "is_private"), true)
        .put(Pair.of(WellKnown.UPDATE_ORDER_REQUEST_MODEL_NAME, "folders"), "[]")
        .build();


  public String getDefaultValue(CodegenModel model, CodegenProperty property) {
      return Optional.ofNullable(MODEL_PROPERTY_DEFAULT_VALUES.get(Pair.of(model.classname, property.name)))
          .map(Object::toString)
          .orElseGet(() -> Optional.ofNullable(GLOBAL_DEFAULT_VALUES.get(property.name))
              .map(Object::toString)
              .orElseGet(() -> wrapWithSingleQuotes(StringUtils.EMPTY)));
  }

}
