package com.nodejs.codegen.utils;

import io.swagger.codegen.v3.generators.DefaultCodegenConfig;

public class StringUtils {

    public static String wrapWithSingleQuotes(String str) {
        return "'" + str + "'";
    }

    public static String toCamelCase(String str) {
        return DefaultCodegenConfig.camelize(str, true);
    }

    public static String toPascalCase(String str) {
        return DefaultCodegenConfig.camelize(str, false);
    }

    private StringUtils() {

    }

}
