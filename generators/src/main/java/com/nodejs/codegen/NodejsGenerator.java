package com.nodejs.codegen;

import com.google.common.collect.ImmutableList;
import com.nodejs.codegen.model.FormParamsModelEnhancer;
import com.nodejs.codegen.model.ModelEnhancer;
import com.nodejs.codegen.model.ProvideValuesForRequestModelEnhancer;
import com.nodejs.codegen.operation.*;
import com.nodejs.codegen.property.*;
import io.swagger.codegen.v3.*;
import io.swagger.codegen.v3.generators.DefaultCodegenConfig;
import io.swagger.v3.oas.models.Operation;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.media.MediaType;
import io.swagger.v3.oas.models.media.Schema;
import io.swagger.v3.oas.models.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;

public class NodejsGenerator extends DefaultCodegenConfig {

  private static final Logger log = LoggerFactory.getLogger(NodejsGenerator.class);

  private static final Pattern OPERATION_ID_PATTERN = Pattern.compile("(.+)(Get|Put|Post)");

  private static final List<OperationsEnhancer> OPERATIONS_ENHANCERS = ImmutableList.<OperationsEnhancer>builder()
      .add(new RequiresOperationsEnhancer())
      .add(new ComplexPropertyEnhancer())
      .add(new PropertyDefaultValueExpressionEnhancer())
      .add(new FormParameterNameOperationsEnhancer())
      .add(new TestLambdaOperationEnhancer())
      .add(new HelperFunctionDefinitionsEnhancer())
      .add(new OperationPrefixEnhancer())
      .add(new FormParamsModelEnhancer())
      .build();

  private static final List<ModelEnhancer> MODEL_ENHANCERS = ImmutableList.<ModelEnhancer>builder()
      .add(new ProvideValuesForRequestModelEnhancer())
      .build();

  private static final List<PropertyEnhancer> PROPERTY_ENHANCERS = ImmutableList.<PropertyEnhancer>builder()
      .add(new DefaultValuePropertyEnhancer())
      .add(new NameInCamelCasePropertyEnhancer())
      .add(new ModelResponseExpectedValuesEnhancer())
      .add(new FilePropertyEnhancer())
      .add(new ArrayPropertyEnhancer())
      .add(new ShouldExpectPropertyEnhancer())
      .build();

  // source folder where to write the files
  protected String sourceFolder = "src";
  protected String apiVersion = "1.0.0";

  /**
   * Configures the type of generator.
   * 
   * @return  the CodegenType for this generator
   * @see     io.swagger.codegen.CodegenType
   */
  public CodegenType getTag() {
    return CodegenType.CLIENT;
  }

  /**
   * Configures a friendly name for the generator.  This will be used by the generator
   * to select the library with the -l flag.
   * 
   * @return the friendly name for the generator
   */
  public String getName() {
    return "nodejs";
  }

  /**
   * Returns human-friendly help for the generator.  Provide the consumer with help
   * tips, parameters here
   * 
   * @return A string value for the help message
   */
  public String getHelp() {
    return "Generates a nodejs client library.";
  }

  public NodejsGenerator() {
    super();

    // set the output folder here
    outputFolder = "generated-code/nodejs";

    /**
     * Models.  You can write model files using the modelTemplateFiles map.
     * if you want to create one template for file, you can do so here.
     * for multiple files for model, just put another entry in the `modelTemplateFiles` with
     * a different extension
     */
    modelTemplateFiles.put(
      "model.mustache", // the template to use
      ".js");       // the extension for each file to write

    /**
     * Api classes.  You can write classes for each Api file with the apiTemplateFiles map.
     * as with models, add multiple entries with different extensions for multiple files per
     * class
     */
    apiTemplateFiles.put(
      "api.mustache",   // the template to use
      ".js");       // the extension for each file to write
    apiTestTemplateFiles.put(
      "api.test.mustache",   // the template to use
      ".js");       // the extension for each file to write

    /**
     * Template Location.  This is the location which templates will be read from.  The generator
     * will use the resource stream to attempt to read the templates.
     */
    templateDir = "nodejs";

    /**
     * Api Package.  Optional, if needed, this can be used in templates
     */
    apiPackage = "api";

    /**
     * Model Package.  Optional, if needed, this can be used in templates
     */
    modelPackage = "model";

    testPackage = "test";

    /**
     * Reserved words.  Override this with reserved words specific to your language
     */
    reservedWords = new HashSet<String> (
      Arrays.asList(
        "sample1",  // replace with static values
        "sample2")
    );

    /**
     * Additional Properties.  These values can be passed to the templates and
     * are available in models, apis, and supporting files
     */
    additionalProperties.put("apiVersion", apiVersion);

    /**
     * Supporting Files.  You can write single files for the generator with the
     * entire object tree available.  If the input file has a suffix of `.mustache
     * it will be processed by the template engine.  Otherwise, it will be copied
     */
    supportingFiles.add(new SupportingFile("configuration.mustache",   // the input template or file
        sourceFolder + "/" + apiPackage().replace('.', File.separatorChar),                                                       // the destination folder, relative `outputFolder`
      "Configuration.js")                                          // the output file
    );
    supportingFiles.add(new SupportingFile("package-index.mustache", sourceFolder, "package-index.js"));

    /**
     * Language Specific Primitives.  These types will not trigger imports by
     * the client generator
     */
    languageSpecificPrimitives = new HashSet<String>(
      Arrays.asList(
        "Type1",      // replace these with your types
        "Type2")
    );
  }

  /**
   * Escapes a reserved word as defined in the `reservedWords` array. Handle escaping
   * those terms here.  This logic is only called if a variable matches the reserved words
   * 
   * @return the escaped term
   */
  @Override
  public String escapeReservedWord(String name) {
    return "_" + name;  // add an underscore to the name
  }

  /**
   * Location to write model files.  You can use the modelPackage() as defined when the class is
   * instantiated
   */
  public String modelFileFolder() {
    return outputFolder + "/" + sourceFolder + "/" + modelPackage().replace('.', File.separatorChar);
  }

  /**
   * Location to write api files.  You can use the apiPackage() as defined when the class is
   * instantiated
   */
  @Override
  public String apiFileFolder() {
    return outputFolder + "/" + sourceFolder + "/" + apiPackage().replace('.', File.separatorChar);
  }

  @Override
  public String apiTestFileFolder() {
    return outputFolder + "/" + sourceFolder + "/" + testPackage().replace('.', File.separatorChar);
  }

  @Override
  public String getArgumentsLocation() {
    return null;
  }

  @Override
  protected String getTemplateDir() {
    return templateDir;
  }

  @Override
  public String getDefaultTemplateDir() {
    return templateDir;
  }

  @Override
  public String toApiName(String name) {
    String apiName = "CallRecorderApi";
    return name.toLowerCase().contains("test") 
      ? apiName + "Test" 
      : apiName;
  }

  @Override
  public String toApiTestFilename(String name) {
    return toApiName(name) + ".test";
  }

  @Override
  public void postProcessModelProperty(CodegenModel model, CodegenProperty property) {
    PROPERTY_ENHANCERS.stream()
        .filter(enhancer -> enhancer.canEnhance(model, property))
        .forEach(propertyEnhancer -> propertyEnhancer.enhance(model, property));
    super.postProcessModelProperty(model, property);
  }

  @Deprecated
  @Override
  public Map<String, Object> postProcessOperations(Map<String, Object> objs) {
    objs = super.postProcessOperations(objs);
    Map<String, Object> operations = (Map<String, Object>) objs.get("operations");
    List<CodegenOperation> operationList = (List<CodegenOperation>) operations.get("operation");
    operationList.forEach(this::postProcessOperation);
    return objs;
  }

  private void postProcessOperation(CodegenOperation operation) {
    PathItem pathItem = openAPI.getPaths().get(operation.path);
    if (pathItem == null) {
      throw new IllegalStateException("Path item was not found: path=" + operation.path);
    }
    Operation pathItemOperation = getPathItemOperation(pathItem)
        .orElseThrow(() -> new IllegalStateException("Failed to extract operation from path item: operation=" + operation));
    String requestModel = extractModelNameFromRef(pathItemOperation.getRequestBody().get$ref());
    String responseModel = getPathItemApiResponse(pathItemOperation)
        .map(ApiResponse::getContent)
        .map(content -> content.entrySet().stream().findFirst().get().getValue())
        .map(MediaType::getSchema)
        .map(Schema::get$ref)
        .map(NodejsGenerator::extractModelNameFromRef)
        .orElseThrow(() -> new IllegalStateException("Failed to extract response model: operationId=" + operation.operationId));
    operation.vendorExtensions.put("x-request-model-name", requestModel);
    operation.vendorExtensions.put("x-response-model-name", responseModel);
    operation.vendorExtensions.put("x-operation-name", getOperationName(operation));
  }

  @Override
  public Map<String, Object> postProcessOperationsWithModels(Map<String, Object> objs, List<Object> allModels) {
    objs = super.postProcessOperationsWithModels(objs, allModels);
    return postProcessOperationsWithModelsExtra(objs, allModels);
  }

  private Map<String, Object> postProcessOperationsWithModelsExtra(Map<String, Object> objs, List<Object> allModels) {
    OperationsContext operationsContext = OperationsContext.fromOperationsAndModels(objs, allModels);
    operationsContext.getOperations().forEach(operation -> postProcessOperation(operation, operationsContext.getModels()));
    operationsContext.getModels().values().forEach(model -> MODEL_ENHANCERS.forEach(modelEnhancer -> modelEnhancer.enhance(model)));
    OPERATIONS_ENHANCERS.forEach(enhancer -> enhancer.enhance(operationsContext));
    return objs;
  }

  private void postProcessOperation(CodegenOperation operation, Map<String, CodegenModel> modelByName) {
    PathItem pathItem = openAPI.getPaths().get(operation.path);
    if (pathItem == null) {
      throw new IllegalStateException("Path item was not found: path=" + operation.path);
    }
    Operation pathItemOperation = getPathItemOperation(pathItem)
        .orElseThrow(() -> new IllegalStateException("Failed to extract operation from path item: operation=" + operation));
    String requestModel = extractModelNameFromRef(pathItemOperation.getRequestBody().get$ref());
    String responseModel = getPathItemApiResponse(pathItemOperation)
        .map(ApiResponse::getContent)
        .map(content -> content.entrySet().stream().findFirst().get().getValue())
        .map(MediaType::getSchema)
        .map(Schema::get$ref)
        .map(NodejsGenerator::extractModelNameFromRef)
        .orElseThrow(() -> new IllegalStateException("Failed to extract response model: operationId=" + operation.operationId));
    operation.vendorExtensions.put("x-request-model", requireNonNull(modelByName.get(requestModel)));
    operation.vendorExtensions.put("x-response-model", requireNonNull(modelByName.get(responseModel)));
    operation.vendorExtensions.put("x-operation-name", getOperationName(operation));
  }

  private static Optional<Operation> getPathItemOperation(PathItem pathItem) {
    if (pathItem.getGet() != null) {
      return Optional.of(pathItem.getGet());
    }
    if (pathItem.getPost() != null) {
      return Optional.of(pathItem.getPost());
    }
    if (pathItem.getPut() != null) {
      return Optional.of(pathItem.getPut());
    }
    return Optional.empty();
  }

  private static Optional<ApiResponse> getPathItemApiResponse(Operation pathItemOperation) {
    ApiResponse successResponse = pathItemOperation.getResponses().get("200");
    log.info("Success response: {}", successResponse);
    return Optional.ofNullable(successResponse);
  }

  private static String extractModelNameFromRef(String ref) {
    return ref.substring(ref.lastIndexOf("/") + 1);
  }

  private static String getOperationName(CodegenOperation operation) {
    Matcher matcher = OPERATION_ID_PATTERN.matcher(operation.operationId);
    if (!matcher.matches()) {
      throw new IllegalArgumentException("Unexpected operation ID format: operationId=" + operation.operationId);
    }
    return matcher.group(1);
  }

  @Override
  public Map<String, Object> postProcessSupportingFileData(Map<String, Object> objs) {
    return super.postProcessSupportingFileData(objs);
  }
}