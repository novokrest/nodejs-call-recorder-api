package com.nodejs.codegen.extensions;

import com.nodejs.codegen.dsl.Expression;
import io.swagger.codegen.v3.CodegenProperty;

public class CodegenPropertyEx {

    public static void setExpectedValue(CodegenProperty property, String value) {
        property.vendorExtensions.put("x-expected-value", value);
    }

    public static String getExpectedValue(CodegenProperty property) {
        return (String) property.vendorExtensions.get("x-expected-value");
    }

    public static void setDefaultValueExpression(CodegenProperty property, Expression defaultValue) {
        property.vendorExtensions.put("x-default-value", defaultValue);
    }

    public static Expression getDefaultValueExpression(CodegenProperty property) {
        return (Expression) property.vendorExtensions.get("x-default-value");
    }

    public static void setComplex(CodegenProperty property, boolean isComplex) {
        property.vendorExtensions.put("x-complex", isComplex);
    }

    public static boolean getComplex(CodegenProperty property) {
        return (boolean) property.vendorExtensions.getOrDefault("x-complex", false);
    }

    public static void setFile(CodegenProperty property, boolean isFile) {
        property.vendorExtensions.put("x-file", isFile);
    }

    public static boolean getFile(CodegenProperty property) {
        return (boolean) property.vendorExtensions.getOrDefault("x-file", false);
    }

    public static void setArray(CodegenProperty property, boolean isArray) {
        property.vendorExtensions.put("x-array", isArray);
    }

    private CodegenPropertyEx() {

    }

    public static boolean getShouldExpect(CodegenProperty property) {
        return (boolean) property.vendorExtensions.get("x-should-expect");
    }

    public static void setShouldExpect(CodegenProperty property, boolean isShouldExpect) {
        property.vendorExtensions.put("x-should-expect", isShouldExpect);
    }

    public static boolean isArray(CodegenProperty property) {
        return property.baseType.equals("array");
    }

}
