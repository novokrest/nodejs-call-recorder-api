package com.nodejs.codegen.extensions;

import com.nodejs.codegen.model.FormParamsModelEnhancer;
import io.swagger.codegen.v3.CodegenModel;

import java.util.List;
import java.util.Map;

import static java.util.Collections.emptyList;

public class CodegenModelEx {

    public static void setHasExtParams(CodegenModel model, boolean hasExtParams) {
        model.vendorExtensions.put("x-has-ext-params", hasExtParams);
    }

    public static boolean getHasExtParams(CodegenModel model) {
        return (boolean) model.vendorExtensions.getOrDefault("x-has-ext-params", false);
    }

    public static Map<String, String> getExtParamNames(CodegenModel model) {
        return (Map<String, String>) model.vendorExtensions.getOrDefault("x-ext-params", emptyList());
    }

    /**
     * @param extParams (property_name -> ext_param_name)
     */
    public static void setExtParams(CodegenModel model, Map<String, String> extParams) {
        model.vendorExtensions.put("x-ext-params", extParams);
    }

    public static void setExtParamFunctionProviderName(CodegenModel model, String functionName) {
        model.vendorExtensions.put("x-ext-params-function-provider-name", functionName);
    }

    public static String getExtParamFunctionProviderName(CodegenModel model) {
        return (String) model.vendorExtensions.get("x-ext-params-function-provider-name");
    }

    public static void setFormParams(CodegenModel model, List<FormParamsModelEnhancer.FormParam> formParams) {
        model.vendorExtensions.put("x-form-params", formParams);
    }

    private CodegenModelEx() {

    }

}
