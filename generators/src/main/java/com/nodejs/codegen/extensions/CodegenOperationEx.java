package com.nodejs.codegen.extensions;

import io.swagger.codegen.v3.CodegenModel;
import io.swagger.codegen.v3.CodegenOperation;

public class CodegenOperationEx {

    public static String getOperationName(CodegenOperation operation) {
        return (String) operation.vendorExtensions.get("x-operation-name");
    }

    public static void setOperationName(CodegenOperation operation, String operationName) {
        operation.vendorExtensions.put("x-operation-name", operationName);
    }

    public static CodegenModel getOperationRequestModel(CodegenOperation operation) {
        return (CodegenModel) operation.vendorExtensions.get("x-request-model");
    }

    public static void setOperationRequestModel(CodegenOperation operation, CodegenModel model) {
        operation.vendorExtensions.put("x-request-model", model);
    }

    public static CodegenModel getOperationResponseModel(CodegenOperation operation) {
        return (CodegenModel) operation.vendorExtensions.get("x-response-model");
    }

    public static void setOperationResponseModel(CodegenOperation operation, CodegenModel model) {
        operation.vendorExtensions.put("x-response-model", model);
    }

    public static void setFormParameterName(CodegenOperation operation, String formParameterName) {
        operation.vendorExtensions.put("x-form-parameter-name", formParameterName);
    }

    public static void setPrefixPath(CodegenOperation operation, String prefixPath) {
        operation.vendorExtensions.put("x-prefix-path", prefixPath);
    }
}
