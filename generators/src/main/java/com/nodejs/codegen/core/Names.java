package com.nodejs.codegen.core;

import com.nodejs.codegen.utils.StringUtils;

import java.util.Collection;
import java.util.stream.Collectors;

public class Names {

    public static String getProvideValuesFunctionName(Collection<String> providedValueNames) {
        return "provide" + providedValueNames.stream()
            .sorted()
            .map(StringUtils::toPascalCase)
            .collect(Collectors.joining("And"));
    }

}
