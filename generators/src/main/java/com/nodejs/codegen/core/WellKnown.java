package com.nodejs.codegen.core;

public interface WellKnown {

    String COMMON_REQUEST_API_KEY_PROPERTY_NAME = "api_key";
    String COMMON_REQUEST_ID_PROPERTY_NAME = "id";
    String COMMON_REQUEST_PARENT_ID_PROPERTY_NAME = "parent_id";
    String COMMON_REQUEST_FILE_PROPERTY_NAME = "file";
    String COMMON_REQUEST_DATA_PROPERTY_NAME = "data";
    String COMMON_RESPONSE_MSG_PROPERTY_NAME = "msg";
    String COMMON_RESPONSE_CODE_PROPERTY_NAME = "code";

    String REGISTER_PHONE_OPERATION_ID = "registerPhonePost";
    String REGISTER_PHONE_RESPONSE_CODE_PROPERTY_NAME = "code";

    String VERIFY_PHONE_OPERATION_ID = "verifyPhonePost";
    String VERIFY_PHONE_REQUEST_MODEL_NAME = "VerifyPhoneRequest";
    String VERIFY_PHONE_REQUEST_CODE_PROPERTY_NAME = "code";
    String VERIFY_PHONE_RESPONSE_API_KEY_PROPERTY_NAME = "api_key";

    String CREATE_FILE_OPERATION_ID = "createFilePost";
    String CREATE_FILE_REQUEST_MODEL_NAME = "CreateFileRequest";
    String CREATE_FILE_DATA_MODEL_NAME = "CreateFileData";
    String CREATE_FILE_REQUEST_DATA_PROPERTY_NAME = "data";
    String CREATE_FILE_REQUEST_FILE_PROPERTY_NAME = "file";
    String CREATE_FILE_RESPONSE_FILE_ID_PROPERTY_NAME = "id";

    String CLONE_FILE_REQUEST_MODEL_NAME = "CloneFileRequest";
    String CLONE_FILE_REQUEST_ID_PROPERTY_NAME = "id";

    String DELETE_FILES_REQUEST_MODEL_NAME = "DeleteFilesRequest";
    String DELETE_FILES_REQUEST_IDS_PROPERTY_NAME = "ids";

    String CREATE_FOLDER_OPERATION_ID = "createFolderPost";
    String CREATE_FOLDER_REQUEST_MODEL_NAME = "CreateFolderRequest";
    String CREATE_FOLDER_RESPONSE_FOLDER_ID_PROPERTY_NAME = "id";

    String DELETE_FOLDER_REQUEST_MODEL_NAME = "DeleteFolderRequest";
    String DELETE_FOLDER_REQUEST_ID_PROPERTY_NAME = "id";

    String UPLOAD_META_FILE_OPERATION_ID = "uploadMetaFilePost";
    String UPLOAD_META_FILE_REQUEST_MODEL_NAME = "UploadMetaFileRequest";
    String UPLOAD_META_FILE_REQUEST_FILE_PROPERTY_NAME = "file";
    String UPLOAD_META_FILE_REQUEST_PARENT_ID_PROPERTY_NAME = "parent_id";
    String UPLOAD_META_FILE_RESPONSE_ID_PROPERTY_NAME = "id";

    String DELETE_META_FILES_REQUEST_MODEL_NAME = "DeleteMetaFilesRequest";
    String DELETE_META_FILES_REQUEST_IDS_PROPERTY_NAME = "ids";
    String DELETE_META_FILES_REQUEST_PARENT_ID_PROPERTY_NAME = "parent_id";

    String GET_FILES_REQUEST_MODEL_NAME = "GetFilesRequest";

    String GET_META_FILES_REQUEST_MODEL_NAME = "GetMetaFilesRequest";

    String GET_LANGUAGES_REQUEST_MODEL_NAME = "GetLanguagesRequest";
    String GET_LANGUAGES_RESPONSE_MODEL_NAME = "GetLanguagesResponse";

    String GET_TRANSLATIONS_REQUEST_MODEL_NAME = "GetTranslationsRequest";
    String GET_TRANSLATIONS_RESPONSE_MODEL_NAME = "GetTranslationsResponse";

    String NOTIFY_USER_REQUEST_MODEL_NAME = "NotifyUserRequest";

    String RECOVER_FILE_REQUEST_MODEL_NAME = "RecoverFileRequest";
    String RECOVER_FILE_REQUEST_FOLDER_ID_PROPERTY_NAME = "folder_id";

    String VERIFY_FOLDER_PASS_REQUEST_MODEL_NAME = "VerifyFolderPassRequest";

    String UPDATE_STAR_REQUEST_MODEL_NAME = "UpdateStarRequest";

    String UPDATE_SETTINGS_REQUEST_MODEL_NAME = "UpdateSettingsRequest";

    String UPDATE_PROFILE_IMAGE_REQUEST_MODEL_NAME = "UpdateProfileImgRequest";

    String UPDATE_FILE_REQUEST_MODEL_NAME = "UpdateFileRequest";

    String UPDATE_FOLDER_REQUEST_MODEL_NAME = "UpdateFolderRequest";

    String UPDATE_ORDER_REQUEST_MODEL_NAME = "UpdateOrderRequest";

    String UPDATE_PROFILE_REQUEST_MODEL_NAME = "UpdateProfileRequest";
    String UPDATE_PROFILE_REQUEST_DATA_MODEL_NAME = "UpdateProfileRequestData";
}
