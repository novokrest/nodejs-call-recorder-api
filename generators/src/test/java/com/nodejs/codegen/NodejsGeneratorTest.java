package com.nodejs.codegen;

import com.google.common.collect.ImmutableMap;
import io.swagger.codegen.v3.CodegenConstants;
import io.swagger.codegen.v3.cli.cmd.Generate;
import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileUtils;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

public class NodejsGeneratorTest {

    private static final Map<String, String> SYSTEM_PROPERTIES = ImmutableMap.<String, String>builder()
        .put("debugOperations", "true")
        .put(CodegenConstants.GENERATE_APIS, "true")
        .put(CodegenConstants.GENERATE_MODELS, "true")
        .put(CodegenConstants.SUPPORTING_FILES, "true")
        .put(CodegenConstants.GENERATE_API_TESTS, "true")
        .build();

    private static final String SPEC_PATH = "../docs/spec.yml";
    private static final String OUTPUT_DIR_PATH = "target/tmp";
    private static final Map<String, String> CODEGEN_OPTIONS = ImmutableMap.<String, String>builder()
        .put("output", OUTPUT_DIR_PATH)
        .put("lang", "nodejs")
        .put("spec", SPEC_PATH)
        .build();

    @BeforeClass
    public static void beforeClass() throws IOException {
        FileUtils.deleteDirectory(new File(OUTPUT_DIR_PATH));
    }

    @Test
    public void test() throws InvocationTargetException, IllegalAccessException {
        SYSTEM_PROPERTIES.forEach(System::setProperty);
        Generate command = new Generate();
        BeanUtils.populate(command, CODEGEN_OPTIONS);
        command.run();
    }

}
