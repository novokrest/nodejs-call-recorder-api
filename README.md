# nodejs-call-recorder-api

## How To

To install `call-recorder-api` package issue following command:
```bash
npm i call-recorder-api
```

## Example

The `call-recorder-api` package exports `CallRecorderApi`, `Configuration` and request/response DTO classes. 

```javascript
const cra = require('call-recorder-api');

const config = cra.Configuration.builder()
    .baseUrl('https://app2.virtualbrix.net')
    .build();
const api = new cra.CallRecorderApi(config);

api.registerPhone(
    cra.RegisterPhoneRequest.builder()
        .token('55942ee3894f51000530894')
        .phone('+16463742122')
        .build(),
    response => {
        console.log('Status: %s, phone: %s, verification code: %s', response.status, response.phone, response.code);
    }
);
```

