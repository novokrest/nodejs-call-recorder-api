const CallRecorderApi = require('./src/main/CallRecorderApi');
const Configuration = require('./src/main/Configuration');
const App = require('./src/main/model/App');
const BuyCreditsRequest = require('./src/main/model/BuyCreditsRequest');
const BuyCreditsResponse = require('./src/main/model/BuyCreditsResponse');
const CloneFileRequest = require('./src/main/model/CloneFileRequest');
const CloneFileResponse = require('./src/main/model/CloneFileResponse');
const CreateFileData = require('./src/main/model/CreateFileData');
const CreateFileRequest = require('./src/main/model/CreateFileRequest');
const CreateFileResponse = require('./src/main/model/CreateFileResponse');
const CreateFolderRequest = require('./src/main/model/CreateFolderRequest');
const CreateFolderResponse = require('./src/main/model/CreateFolderResponse');
const DeleteFilesRequest = require('./src/main/model/DeleteFilesRequest');
const DeleteFilesResponse = require('./src/main/model/DeleteFilesResponse');
const DeleteFolderRequest = require('./src/main/model/DeleteFolderRequest');
const DeleteFolderResponse = require('./src/main/model/DeleteFolderResponse');
const DeleteMetaFilesRequest = require('./src/main/model/DeleteMetaFilesRequest');
const DeleteMetaFilesResponse = require('./src/main/model/DeleteMetaFilesResponse');
const DeviceType = require('./src/main/model/DeviceType');
const FilesPermission = require('./src/main/model/FilesPermission');
const GetFilesRequest = require('./src/main/model/GetFilesRequest');
const GetFilesResponse = require('./src/main/model/GetFilesResponse');
const GetFilesResponseFile = require('./src/main/model/GetFilesResponseFile');
const GetFoldersRequest = require('./src/main/model/GetFoldersRequest');
const GetFoldersResponse = require('./src/main/model/GetFoldersResponse');
const GetFoldersResponseFolder = require('./src/main/model/GetFoldersResponseFolder');
const GetLanguagesRequest = require('./src/main/model/GetLanguagesRequest');
const GetLanguagesResponse = require('./src/main/model/GetLanguagesResponse');
const GetMessagesRequest = require('./src/main/model/GetMessagesRequest');
const GetMessagesResponse = require('./src/main/model/GetMessagesResponse');
const GetMessagesResponseMsg = require('./src/main/model/GetMessagesResponseMsg');
const GetMetaFilesRequest = require('./src/main/model/GetMetaFilesRequest');
const GetMetaFilesResponse = require('./src/main/model/GetMetaFilesResponse');
const GetMetaFilesResponseMetaFiles = require('./src/main/model/GetMetaFilesResponseMetaFiles');
const GetPhonesRequest = require('./src/main/model/GetPhonesRequest');
const GetPhonesResponse = require('./src/main/model/GetPhonesResponse');
const GetPhonesResponsePhone = require('./src/main/model/GetPhonesResponsePhone');
const GetProfileRequest = require('./src/main/model/GetProfileRequest');
const GetProfileResponse = require('./src/main/model/GetProfileResponse');
const GetProfileResponseProfile = require('./src/main/model/GetProfileResponseProfile');
const GetSettingsRequest = require('./src/main/model/GetSettingsRequest');
const GetSettingsResponse = require('./src/main/model/GetSettingsResponse');
const GetSettingsResponseSettings = require('./src/main/model/GetSettingsResponseSettings');
const GetTranslationsRequest = require('./src/main/model/GetTranslationsRequest');
const GetTranslationsResponse = require('./src/main/model/GetTranslationsResponse');
const Language = require('./src/main/model/Language');
const NotifyUserRequest = require('./src/main/model/NotifyUserRequest');
const NotifyUserResponse = require('./src/main/model/NotifyUserResponse');
const PlayBeep = require('./src/main/model/PlayBeep');
const RecoverFileRequest = require('./src/main/model/RecoverFileRequest');
const RecoverFileResponse = require('./src/main/model/RecoverFileResponse');
const RegisterPhoneRequest = require('./src/main/model/RegisterPhoneRequest');
const RegisterPhoneResponse = require('./src/main/model/RegisterPhoneResponse');
const UpdateDeviceTokenRequest = require('./src/main/model/UpdateDeviceTokenRequest');
const UpdateDeviceTokenResponse = require('./src/main/model/UpdateDeviceTokenResponse');
const UpdateFileRequest = require('./src/main/model/UpdateFileRequest');
const UpdateFileResponse = require('./src/main/model/UpdateFileResponse');
const UpdateFolderRequest = require('./src/main/model/UpdateFolderRequest');
const UpdateFolderResponse = require('./src/main/model/UpdateFolderResponse');
const UpdateOrderRequest = require('./src/main/model/UpdateOrderRequest');
const UpdateOrderRequestFolder = require('./src/main/model/UpdateOrderRequestFolder');
const UpdateOrderResponse = require('./src/main/model/UpdateOrderResponse');
const UpdateProfileImgRequest = require('./src/main/model/UpdateProfileImgRequest');
const UpdateProfileImgResponse = require('./src/main/model/UpdateProfileImgResponse');
const UpdateProfileRequest = require('./src/main/model/UpdateProfileRequest');
const UpdateProfileRequestData = require('./src/main/model/UpdateProfileRequestData');
const UpdateProfileResponse = require('./src/main/model/UpdateProfileResponse');
const UpdateSettingsRequest = require('./src/main/model/UpdateSettingsRequest');
const UpdateSettingsResponse = require('./src/main/model/UpdateSettingsResponse');
const UpdateStarRequest = require('./src/main/model/UpdateStarRequest');
const UpdateStarResponse = require('./src/main/model/UpdateStarResponse');
const UpdateUserRequest = require('./src/main/model/UpdateUserRequest');
const UpdateUserResponse = require('./src/main/model/UpdateUserResponse');
const UploadMetaFileRequest = require('./src/main/model/UploadMetaFileRequest');
const UploadMetaFileResponse = require('./src/main/model/UploadMetaFileResponse');
const VerifyFolderPassRequest = require('./src/main/model/VerifyFolderPassRequest');
const VerifyFolderPassResponse = require('./src/main/model/VerifyFolderPassResponse');
const VerifyPhoneRequest = require('./src/main/model/VerifyPhoneRequest');
const VerifyPhoneResponse = require('./src/main/model/VerifyPhoneResponse');

module.exports = {
    CallRecorderApi,
    Configuration,
    App,
    BuyCreditsRequest,
    BuyCreditsResponse,
    CloneFileRequest,
    CloneFileResponse,
    CreateFileData,
    CreateFileRequest,
    CreateFileResponse,
    CreateFolderRequest,
    CreateFolderResponse,
    DeleteFilesRequest,
    DeleteFilesResponse,
    DeleteFolderRequest,
    DeleteFolderResponse,
    DeleteMetaFilesRequest,
    DeleteMetaFilesResponse,
    DeviceType,
    FilesPermission,
    GetFilesRequest,
    GetFilesResponse,
    GetFilesResponseFile,
    GetFoldersRequest,
    GetFoldersResponse,
    GetFoldersResponseFolder,
    GetLanguagesRequest,
    GetLanguagesResponse,
    GetMessagesRequest,
    GetMessagesResponse,
    GetMessagesResponseMsg,
    GetMetaFilesRequest,
    GetMetaFilesResponse,
    GetMetaFilesResponseMetaFiles,
    GetPhonesRequest,
    GetPhonesResponse,
    GetPhonesResponsePhone,
    GetProfileRequest,
    GetProfileResponse,
    GetProfileResponseProfile,
    GetSettingsRequest,
    GetSettingsResponse,
    GetSettingsResponseSettings,
    GetTranslationsRequest,
    GetTranslationsResponse,
    Language,
    NotifyUserRequest,
    NotifyUserResponse,
    PlayBeep,
    RecoverFileRequest,
    RecoverFileResponse,
    RegisterPhoneRequest,
    RegisterPhoneResponse,
    UpdateDeviceTokenRequest,
    UpdateDeviceTokenResponse,
    UpdateFileRequest,
    UpdateFileResponse,
    UpdateFolderRequest,
    UpdateFolderResponse,
    UpdateOrderRequest,
    UpdateOrderRequestFolder,
    UpdateOrderResponse,
    UpdateProfileImgRequest,
    UpdateProfileImgResponse,
    UpdateProfileRequest,
    UpdateProfileRequestData,
    UpdateProfileResponse,
    UpdateSettingsRequest,
    UpdateSettingsResponse,
    UpdateStarRequest,
    UpdateStarResponse,
    UpdateUserRequest,
    UpdateUserResponse,
    UploadMetaFileRequest,
    UploadMetaFileResponse,
    VerifyFolderPassRequest,
    VerifyFolderPassResponse,
    VerifyPhoneRequest,
    VerifyPhoneResponse,
};
